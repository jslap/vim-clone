//
// Created by Jean-Simon Lapointe on 2020-09-05.
//

#ifndef MYVIM_BUFFER_H
#define MYVIM_BUFFER_H

#include "VimUtil.h"
#include <filesystem>
#include <optional>
#include <regex>

class Buffer {
public:
    Buffer() {}
    Buffer(std::filesystem::path const & p);

    bool hasPath() const {return !mBasePath.empty();}
    std::filesystem::path const &getPath() const;
    bool save() const;
    bool load();
    bool isOutdated() const { return mIsOutdated;}

    // Non modifying text methods
    bool endsWithNewLine() const {return !mContent.empty() && mContent.back()=='\n';}
    std::string getLine(int lineNum) const;
    int nbLine() const;
    std::pair<int, int> getLineNumAndOffsetForBufferOffset(int bufOffset) const;
    int getBufferOffsetFromStartOfLine(int lineNum) const;
    BufferPos getBufferLength() const {return mContent.length();}

    BufferPos getBufferPosOfPosLineStart(BufferPos pos) const;
    BufferPos getBufferPosOfPosLineEnd(BufferPos pos) const;

    std::optional<u64> match(std::regex const &pattern, u64 fromOffset = 0, u64 numSubMatch = 0) const;

    // Modifying methods
    void deleteChunk(BufferPos posStart, BufferPos posEnd);

    // slow function, should avoid to use this in apps.
    std::string getContent() const;
    void setContent(std::string const &content);

    int getLineLength(int lineNum);

private:
    std::filesystem::path mBasePath{};
    std::string mContent{};
    mutable bool mIsOutdated{false};
};


#endif //MYVIM_BUFFER_H
