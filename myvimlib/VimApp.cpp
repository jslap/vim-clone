//
// Created by Jean-Simon Lapointe on 2020-09-06.
//

#include "VimApp.h"

#define FMT_STRING_ALIAS 1

#include <range/v3/view.hpp>
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ranges.h>
#include "spdlog/spdlog.h"

VimApp::VimApp(int ncol, int nline, std::filesystem::path const &p /*= {}*/) :
        mNumCol{ncol},
        mNumLine{nline},
        mBuffer{p}
{
    spdlog::info("Welcome to VimApp! {} Lines {} cols. Path({})", ncol, nline, p.c_str());
    refreshScreen();
}

void VimApp::setCursorBufferPosAndRefresh(BufferPos pos)
{
    if (pos > getMaxBufferPos())
        throw std::logic_error{"Outside of buffer."};
    mCursorBufferPos = pos;
    refreshScreen();
}

void VimApp::setScreenStartLineAndRefresh(int startLine)
{
    mScreenStartLineOffset = startLine;
    refreshScreen();
}

void VimApp::refreshScreen()
{
    int lineHeaderSize = getLineHeaderSize();

    auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);

    mCursorScreenPos.x = lineHeaderSize + offsetInLine;
    mCursorScreenPos.y = cursorLineNum - mScreenStartLineOffset;

    int statusBarNbLine = 1;
    int commandLineNbLine = 1;
    int numberOfScreenLineForBuffer = mNumLine - (statusBarNbLine + commandLineNbLine);

    mScreen.mLines.clear();
    int lineStart = mScreenStartLineOffset;
    int lineEnd = lineStart + numberOfScreenLineForBuffer - 1;
    for (int l = lineStart; l <= lineEnd; l++)
    {
        ScreenLine curScreenLine;
        if (l < mBuffer.nbLine())
        {
            curScreenLine.mStr = fmt::format("{0:>{2}} {1}", l + 1, mBuffer.getLine(l), lineHeaderSize - 1);
        }
        mScreen.mLines.push_back(curScreenLine);
    }

    mScreen.mLines.push_back(getStatusLine());
    mScreen.mLines.push_back(getCommandLine());

    if (mCursorScreenPos.x < lineHeaderSize)
        mCursorScreenPos.x = lineHeaderSize;
    if (mCursorScreenPos.x >= mNumCol)
        mCursorScreenPos.x = mNumCol - 1;

    if (mCursorScreenPos.y < 0)
        mCursorScreenPos.y = 0;
    if (mCursorScreenPos.y >= mNumLine)
        mCursorScreenPos.y = mNumLine - 1;
}

int VimApp::getLineHeaderSize() const
{
    int nbLineInBuf{mBuffer.nbLine()};
    int nbSpaceForLine = std::to_string(nbLineInBuf + 1).length() + 1;
    return nbSpaceForLine + 1;
}

void VimApp::quit()
{
    spdlog::debug("VimApp: quit");
    mAppIsDone = true;
}

void VimApp::goRight()
{
    auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
    auto curLine = mBuffer.getLine(cursorLineNum);
    if (offsetInLine < curLine.length())
        mCursorBufferPos++;
    makeCursorPosOnScreen();
}

void VimApp::goLeft()
{
    auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
    auto curLine = mBuffer.getLine(cursorLineNum);

    if (offsetInLine > 0)
        mCursorBufferPos--;
    makeCursorPosOnScreen();
}

void VimApp::goUp()
{
    auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
    auto targetCol = getVerticalMoveTargetColumn();

    if (cursorLineNum > 0)
    {
        auto prevLineLength = mBuffer.getLine(cursorLineNum - 1).length();

        mCursorBufferPos -= (prevLineLength + offsetInLine) + 1;
        mCursorBufferPos += targetCol < prevLineLength ? targetCol : prevLineLength;
    }
    makeCursorPosOnScreen();
}

void VimApp::goDown()
{
    auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
    if (cursorLineNum < mBuffer.nbLine() - 1)
    {
        auto targetCol = getVerticalMoveTargetColumn();

        auto curLineLength = mBuffer.getLine(cursorLineNum).length();
        auto nextLineLength = mBuffer.getLine(cursorLineNum + 1).length();
        mCursorBufferPos += (curLineLength - offsetInLine) + 1;
        mCursorBufferPos += targetCol < nextLineLength ? targetCol : nextLineLength;
    }
    makeCursorPosOnScreen();
}

int VimApp::getNumCol() const
{
    return mNumCol;
}

void VimApp::setNumCol(int numCol)
{
    mNumCol = numCol;
}

int VimApp::getNumLine() const
{
    return mNumLine;
}

void VimApp::setNumLine(int numLine)
{
    mNumLine = numLine;
}

VimApp::ScreenPos VimApp::getCursorPos() const
{
    return mCursorScreenPos;
}

int VimApp::getLineOfCursor() const
{
    return mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos).first;
}

ScreenLine VimApp::getStatusLine() const
{
    return {};
}

ScreenLine VimApp::getCommandLine() const
{
    if (!mCurrentError.mStr.empty())
        return mCurrentError;
    return mCommandManager.getCommandLine();
}

int VimApp::getVerticalMoveTargetColumn() const
{
    return mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos).second;
}

void VimApp::makeCursorPosOnScreen()
{
    auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
    // Can't be on the line break itself.
    if (mBuffer.getLineLength(cursorLineNum) > 0 && offsetInLine >= mBuffer.getLineLength(cursorLineNum))
    {
        mCursorBufferPos--;
        offsetInLine--;
    }

    int statusBarNbLine = 1;
    int commandLineNbLine = 1;
    int numberOfScreenLineForBuffer = mNumLine - (statusBarNbLine + commandLineNbLine);

    if (cursorLineNum < mScreenStartLineOffset)
        mScreenStartLineOffset = cursorLineNum;
    if (cursorLineNum > mScreenStartLineOffset + numberOfScreenLineForBuffer - 1)
        mScreenStartLineOffset = cursorLineNum - numberOfScreenLineForBuffer + 1;
}

void VimApp::putChar(CharCode const &c)
{
    auto cmd = mCommandManager.putChar(c);
    if (!mCommandManager.getCommandLine().mStr.empty())
        mCurrentError.mStr.clear();
    executeCommand(cmd);
}

void VimApp::escapeKey()
{
    spdlog::debug("VimApp::escapeKey");
    auto cmd = mCommandManager.escapeKey();
    executeCommand(cmd);
}

void VimApp::rightArrow()
{
    auto cmd = mCommandManager.rightArrow();
    executeCommand(cmd);
}

void VimApp::leftArrow()
{
    auto cmd = mCommandManager.leftArrow();
    executeCommand(cmd);
}

void VimApp::upArrow()
{
    auto cmd = mCommandManager.upArrow();
    executeCommand(cmd);
}

void VimApp::downArrow()
{
    auto cmd = mCommandManager.downArrow();
    executeCommand(cmd);
}

void VimApp::executeCommand(CommandManager::ReturnedCommand const &cmd)
{
    if (cmd.has_value())
    {
        std::visit([this](auto &&theCmd) { executeCommand(theCmd); }, *cmd);
    }
    refreshScreen();
}

void VimApp::executeCommand(CommandManager::MoveCommand const &cmd)
{
    mCursorBufferPos = computePosAfterMove(cmd, false);
    makeCursorPosOnScreen();
}

void VimApp::executeCommand(CommandManager::EditCommand const &editCmd)
{
    auto getStartAndEndFromMove = [&](
            CommandManager::EditCommand const & editCmd,
            CommandManager::MoveCommand const & moveCmd) -> std::pair<BufferPos, BufferPos>
    {
        CommandManager::MoveCommand finalMove = moveCmd;
        finalMove.mRepetition *= editCmd.mRepetition;
        BufferPos startPosition = mCursorBufferPos;
        BufferPos endPosition = computePosAfterMove(finalMove, true);
        if (moveCmd.mCommandType == CommandManager::MoveCommandType::MoveDown || moveCmd.mCommandType == CommandManager::MoveCommandType::MoveUp)
        {
            if (startPosition < endPosition)
            {
                startPosition = mBuffer.getBufferPosOfPosLineStart(startPosition);
                endPosition = mBuffer.getBufferPosOfPosLineEnd(endPosition);
            }
            else
            {
                endPosition = mBuffer.getBufferPosOfPosLineStart(endPosition);
                startPosition = mBuffer.getBufferPosOfPosLineEnd(startPosition);
            }
        }
        return {startPosition, endPosition};
    };
//    std::optional<BufferPos> posAfterMove{};
//    if (editCmd.mApplyTo)
//    {
//        CommandManager::MoveCommand finalMove = *editCmd.mApplyTo;
//        finalMove.mRepetition *= editCmd.mRepetition;
//        posAfterMove = computePosAfterMove(finalMove, true);
//    }
    switch (editCmd.mCommandType)
    {
        case CommandManager::EditCommandType::Delete:
        {
            if (editCmd.mApplyTo)
            {
                auto [deleteStart, deleteEnd] = getStartAndEndFromMove(editCmd, *editCmd.mApplyTo);

                if (deleteEnd > deleteStart)
                    mBuffer.deleteChunk(deleteStart, deleteEnd-1);
                else if (deleteEnd < deleteStart)
                {
                    mBuffer.deleteChunk(deleteEnd, deleteStart-1);
                    mCursorBufferPos = deleteEnd;
                }
                makeCursorPosOnScreen();
            }
            else
                deleteCurrentLine(editCmd.mRepetition);

            break;
        }

    }
}

void VimApp::executeCommand(CommandManager::ApplicationCommand const &cmd)
{
    switch (cmd.mCommandType)
    {
        case CommandManager::AppCommandType::Quit:
        {
            if (mBuffer.isOutdated())
            {
                mCurrentError.mStr = "Error: Buffer is outdated.";
            }
            else
            {
                quit();
            }
            break;
        }
        case CommandManager::AppCommandType::Save:
            mBuffer.save();
            break;
        case CommandManager::AppCommandType::QuitHard:
            quit();
            break;
        case CommandManager::AppCommandType::SaveAndQuit:
        {
            mBuffer.save();
            quit();
        }
        break;
        case CommandManager::AppCommandType::Reload:
        {
            if (mBuffer.isOutdated())
            {
                mCurrentError.mStr = "Error: Buffer is outdated.";
            }
            else
            {
                mBuffer.load();
            }
            break;
        }
        case CommandManager::AppCommandType::ReloadHard:
            mBuffer.load();
            break;
    }
}

BufferPos VimApp::computePosAfterMove(CommandManager::MoveCommand const &cmd, bool countEOL) const
{
    if (mBuffer.nbLine() == 0)
        return 0;
    switch (cmd.mCommandType)
    {
        case CommandManager::MoveCommandType::MoveDown:
        {
            auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
            auto targetLine = cursorLineNum+cmd.mRepetition;
            targetLine = std::min(mBuffer.nbLine() - 1 , targetLine);
            if (targetLine != cursorLineNum)
            {
                auto targetCol = getVerticalMoveTargetColumn();
                auto targetLineLength = mBuffer.getLine(targetLine).length();
                if (targetCol > targetLineLength)
                    targetCol = targetLineLength;

                auto startOfTargetLineOffset = mBuffer.getBufferOffsetFromStartOfLine(targetLine);

                return startOfTargetLineOffset + targetCol;
            }
            return mCursorBufferPos;
        }
        case CommandManager::MoveCommandType::MoveUp:
        {
            auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
            auto targetLine = cursorLineNum-cmd.mRepetition;
            targetLine = std::max(targetLine, 0);
            if (targetLine != cursorLineNum)
            {
                auto targetCol = getVerticalMoveTargetColumn();
                auto targetLineLength = mBuffer.getLine(targetLine).length();
                if (targetCol > targetLineLength)
                    targetCol = targetLineLength;

                auto startOfTargetLineOffset = mBuffer.getBufferOffsetFromStartOfLine(targetLine);

                return startOfTargetLineOffset + targetCol;
            }
            return mCursorBufferPos;
        }
        case CommandManager::MoveCommandType::MoveLeft:
        {
            int remainingRepetion = cmd.mRepetition;
            auto curOffset{mCursorBufferPos};
            while (remainingRepetion > 0 && curOffset > 0)
            {
                auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(curOffset);
                remainingRepetion--;
                curOffset--;
                if (!countEOL && offsetInLine == 0)
                    curOffset--;
            }
            return curOffset;
        }
        case CommandManager::MoveCommandType::MoveRight:
        {
            int remainingRepetion = cmd.mRepetition;
            auto curOffset{mCursorBufferPos};
            auto bufferSize = mBuffer.getBufferLength();
            while (remainingRepetion > 0 && curOffset < (bufferSize-1))
            {
                auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(curOffset);
                auto curLine = mBuffer.getLine(cursorLineNum);
                auto remainingCharOnLine = mBuffer.getLine(cursorLineNum).length()-offsetInLine;
                if (remainingRepetion < remainingCharOnLine)
                {
                    curOffset += remainingRepetion;
                    remainingRepetion = 0;
                }
                else
                {
                    remainingRepetion -= (remainingCharOnLine);
                    curOffset += (remainingCharOnLine);
                    if (!countEOL)
                    {
                        curOffset++;
                    }
                    else if (remainingRepetion > 0)
                    {
                        remainingRepetion--;
                        curOffset++;
                    }
                }
            }
            return std::min(curOffset, getMaxBufferPos()+1);
        }
        case CommandManager::MoveCommandType::MoveToCharInLine:
        {
            auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
            auto curLine = mBuffer.getLine(cursorLineNum);
            auto foundOffset = offsetInLine;
            for (int i = 0 ; i < cmd.mRepetition; i++)
            {
                auto nextFoundOffset = curLine.find_first_of(cmd.mArgument, foundOffset+1);
                if (nextFoundOffset == std::string::npos)
                    break;
                foundOffset = nextFoundOffset;
            }
            return mCursorBufferPos + (foundOffset-offsetInLine);
        }
        case CommandManager::MoveCommandType::MoveToBeginLine:
        {
            auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
            return mCursorBufferPos-offsetInLine;
        }
        case CommandManager::MoveCommandType::MoveToEndLine:
        {
            auto[cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
            auto curLine = mBuffer.getLine(cursorLineNum);
            return (mCursorBufferPos-offsetInLine)+(curLine.length());
        }
        case CommandManager::MoveCommandType::MoveToSearch:
        {
            std::string searchRegExString = cmd.mArgument | ranges::to<std::string>;
            std::regex searchRegEx{searchRegExString};

            auto currentOffset = mCursorBufferPos;
            for (int i = 0 ; i < cmd.mRepetition; i++)
            {
                auto foundOffsetOpt = mBuffer.match(searchRegEx, currentOffset+1);
                if (!foundOffsetOpt.has_value())
                    break;
                currentOffset = *foundOffsetOpt;
            }
            return currentOffset;
        }
        case CommandManager::MoveCommandType::MoveWordFwd:
        {
            // could be optimized with a single search? "\A\s*(?:[^\s]+[\s]+){N}([^\s])"
            //                                                  Nb Repetition ^
            std::regex searchRegEx{"[^\\s]*\\s+([^\\s])"};

            auto currentOffset = mCursorBufferPos;
            for (int i = 0 ; i < cmd.mRepetition; i++)
            {
                auto foundOffsetOpt = mBuffer.match(searchRegEx, currentOffset+1, 1);
                if (!foundOffsetOpt.has_value())
                {
                    currentOffset = getMaxBufferPos()+1;
                    break;
                }
                currentOffset = *foundOffsetOpt;
            }
            return currentOffset;
        }
        case CommandManager::MoveCommandType::MoveWordBack:
            break;
    }
    return mCursorBufferPos;
}

BufferPos VimApp::getMaxBufferPos() const
{
    if (mBuffer.getBufferLength() == 0)
        return 0;
    if (mBuffer.endsWithNewLine())
        return mBuffer.getBufferLength()-2;
    return mBuffer.getBufferLength()-1;
}


void VimApp::deleteCurrentLine(int nbRep)
{
    auto [cursorLineNum, offsetInLine] = mBuffer.getLineNumAndOffsetForBufferOffset(mCursorBufferPos);
    auto nbLineToDelete{nbRep};
    if (nbLineToDelete >= mBuffer.nbLine()-cursorLineNum)
    {
        mBuffer.deleteChunk(mBuffer.getBufferOffsetFromStartOfLine(cursorLineNum),
                            mBuffer.getBufferOffsetFromStartOfLine(mBuffer.nbLine())-1);
        if (cursorLineNum != 0)
            mCursorBufferPos = mBuffer.getBufferOffsetFromStartOfLine(cursorLineNum-1);
        else
            mCursorBufferPos = 0;
    }
    else
    {
        mBuffer.deleteChunk(mBuffer.getBufferOffsetFromStartOfLine(cursorLineNum),
                            mBuffer.getBufferOffsetFromStartOfLine(cursorLineNum+nbRep)-1);
        mCursorBufferPos = mBuffer.getBufferOffsetFromStartOfLine(cursorLineNum);
    }
        nbLineToDelete = mBuffer.nbLine()-cursorLineNum;
}

void VimApp::setFullBuffer(std::string const &content)
{
    mBuffer.setContent(content);
    refreshScreen();
}

void VimApp::deleteInBuffer(BufferPos newPos)
{
    if (newPos > mCursorBufferPos)
        mBuffer.deleteChunk(mCursorBufferPos, newPos-1);
    else if (newPos < mCursorBufferPos)
    {
        mBuffer.deleteChunk(newPos, mCursorBufferPos-1);
        mCursorBufferPos = newPos;
    }
    makeCursorPosOnScreen();
}
