//
// Created by Jean-Simon Lapointe on 2020-09-06.
//

#ifndef MYVIM_VIMAPP_H
#define MYVIM_VIMAPP_H

#include "VimUtil.h"
#include "Buffer.h"
#include "CommandManager.h"

#define FMT_STRING_ALIAS 1
#include <fmt/core.h>

#include <vector>

class VimApp {
public:

    VimApp(int ncol, int nline, std::filesystem::path const &p = {});

    // returns true if the application is done and can now die.
    bool appIsDone() const {return mAppIsDone;}

    int getNumCol() const;
    int getNumLine() const;

    void setNumCol(int numCol);
    void setNumLine(int numLine);

    void putChar(CharCode const & c);
    void escapeKey();
    void rightArrow();
    void leftArrow();
    void upArrow();
    void downArrow();


    //actions
    void quit();
    void goRight();
    void goLeft();
    void goUp();
    void goDown();

    class ScreenPos
    {
    public:
        int x{};
        int y{};

        friend bool operator==(const ScreenPos& lhs, const ScreenPos& rhs)  {
            return lhs.x == rhs.x && lhs.y == rhs.y;
        }
        friend std::ostream& operator << ( std::ostream& os, ScreenPos const& value ) {
            os << fmt::format("ScreenPos({},{})", value.x, value.y);
            return os;
        }
    };
    ScreenPos getCursorPos() const;
    int getLineOfCursor() const;

    class Screen
    {
    public:
        std::vector<ScreenLine> mLines;
    };

    Screen const & getScreen() const { return mScreen; }

    Mode getMode() const {return mCommandManager.getMode();}


    // Used for testing.
    BufferPos getBufferPos() const {return mCursorBufferPos;}
    std::string getFullBuffer() const {return mBuffer.getContent();}
    void setFullBuffer(std::string const & content);
    void setCursorBufferPosAndRefresh(BufferPos pos);
    void setScreenStartLineAndRefresh(int startLine);

    void executeCommand(CommandManager::ReturnedCommand const & cmd);
    void executeCommand(CommandManager::MoveCommand const &  cmd);
    void executeCommand(CommandManager::EditCommand const &  cmd);
    void executeCommand(CommandManager::ApplicationCommand const &  cmd);
    void refreshScreen();

    // Not normally used, but could be usefull for some impl.
    ScreenLine getStatusLine() const;
    ScreenLine getCommandLine() const;
private:

    void makeCursorPosOnScreen();

    BufferPos computePosAfterMove(CommandManager::MoveCommand const &cmd, bool countEOL) const;
    BufferPos getMaxBufferPos() const;


    int getVerticalMoveTargetColumn() const;

    void deleteCurrentLine(int nbRep);

    int mNumCol;
    int mNumLine;
    Buffer mBuffer;

    BufferPos mCursorBufferPos{};
    int mScreenStartLineOffset{};

    Screen mScreen;
    ScreenPos mCursorScreenPos{};

    ScreenLine mCurrentError{};

    CommandManager mCommandManager;
    bool mAppIsDone{false};

    int getLineHeaderSize() const;

    void deleteInBuffer(BufferPos newPos);
};


#endif //MYVIM_VIMAPP_H
