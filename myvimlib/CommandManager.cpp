//
// Created by Jean-Simon Lapointe on 2020-09-13.
//

#include "CommandManager.h"

#define FMT_STRING_ALIAS 1
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ranges.h>
#include "spdlog/spdlog.h"

template<>
struct fmt::formatter<CommandManager::MoveCommand>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {return ctx.begin();}

    template<typename FormatContext>
    auto format(CommandManager::MoveCommand const& cmd, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "Move Command type:{} str:{} rep:{}", cmd.mCommandType, cmd.mCommandString, cmd.mRepetition);
    }
};

template<>
struct fmt::formatter<CommandManager::ApplicationCommand>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {return ctx.begin();}

    template<typename FormatContext>
    auto format(CommandManager::ApplicationCommand const& cmd, FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "Move Command type:{} str:{} rep:{}", cmd.mCommandType, cmd.mCommandString, cmd.mRepetition);
    }
};

template<>
struct fmt::formatter<CommandManager::EditCommand>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {return ctx.begin();}

    template<typename FormatContext>
    auto format(CommandManager::EditCommand const& cmd, FormatContext& ctx)
    {
        if (cmd.mApplyTo)
            return fmt::format_to(ctx.out(), "Edit Command type:{} str:{} rep:{} move:{}", cmd.mCommandType, cmd.mCommandString, cmd.mRepetition, *(cmd.mApplyTo));
        return fmt::format_to(ctx.out(), "Edit Command type:{} str:{} rep:{} Raw move", cmd.mCommandType, cmd.mCommandString, cmd.mRepetition);
    }
};

template<>
struct fmt::formatter<CommandManager::GenCommand>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {return ctx.begin();}

    template<typename FormatContext>
    auto format(CommandManager::GenCommand const& cmd, FormatContext& ctx)
    {
        return std::visit([&ctx](auto c){
            return fmt::format_to(ctx.out(), "{}", c);
        }, cmd);
    }
};

template<>
struct fmt::formatter<CommandManager::ReturnedCommand>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {return ctx.begin();}

    template<typename FormatContext>
    auto format(CommandManager::ReturnedCommand const& cmd, FormatContext& ctx)
    {
        if (!cmd)
            return fmt::format_to(ctx.out(), "Null Command");
        return fmt::format_to(ctx.out(), "{}", *cmd);
    }
};

class FailedParse{};
class UndecidedParse{};
template <typename T>
using ParseTry = std::variant<FailedParse, UndecidedParse, T>;

template<typename T>
bool isValue(ParseTry<T> const &p)
{
    return std::holds_alternative<T>(p);
}

template<typename Out, typename In>
ParseTry<Out> convParse(ParseTry<In> const & p)
{
    if (isValue(p))
        return std::get<In>(p);
    else if (std::holds_alternative<UndecidedParse>(p))
        return UndecidedParse{};
    else
        return FailedParse{};
}

template<>
struct fmt::formatter<FailedParse>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {return ctx.begin();}

    template<typename FormatContext>
    auto format(FailedParse const& , FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "Failed Parse");
    }
};

template<>
struct fmt::formatter<UndecidedParse>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {return ctx.begin();}

    template<typename FormatContext>
    auto format(UndecidedParse const& , FormatContext& ctx)
    {
        return fmt::format_to(ctx.out(), "Undecided Parse");
    }
};

template<typename T>
struct fmt::formatter<ParseTry<T>>
{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {return ctx.begin();}

    template<typename FormatContext>
    auto format(ParseTry<T> const& pt, FormatContext& ctx)
    {
        return std::visit([&ctx](auto c){
            return fmt::format_to(ctx.out(), "Parsed({})", c);
        }, pt);
    }
};

int parseCount(CommandManager::CharVecRange rng)
{
    if (rng.first == rng.second)
        return 1;

    // To be optim
    std::string tmpStr{rng.first, rng.second};
    return std::stol(tmpStr);
}


CommandManager::ReturnedCommand CommandManager::putChar(CharCode const & c )
{
    ReturnedCommand retVal;
    if (mMode == Mode::Normal)
        retVal = normalModePutChar(c);
    else if (mMode == Mode::Command)
        retVal = commandModePutChar(c);
    updateLastEditCommand(retVal);
    return retVal;
}

std::optional<CommandManager::GenCommand> CommandManager::executeCommandBuffer()
{
//    auto cmdMap = {
//            { {}, [](){} }
//    };

    spdlog::debug("commandModePutChar: execute command: {}", mCommandBuffer);
    if (mCommandBuffer == CmdBuffer{'q'})
        return ApplicationCommand{{":q", 1}, AppCommandType::Quit};
    if (mCommandBuffer == CmdBuffer{'w'})
        return ApplicationCommand{{":w", 1}, AppCommandType::Save};
    if (mCommandBuffer == CmdBuffer{'w', 'q'})
        return ApplicationCommand{{":wq", 1}, AppCommandType::SaveAndQuit};
    if (mCommandBuffer == CmdBuffer{'q', '!'})
        return ApplicationCommand{{":q!", 1}, AppCommandType::QuitHard};
    if (mCommandBuffer == CmdBuffer{'e', '!'})
        return ApplicationCommand{{":e!", 1}, AppCommandType::ReloadHard};
    if (mCommandBuffer == CmdBuffer{'e'})
        return ApplicationCommand{{":e", 1}, AppCommandType::Reload};

    spdlog::warn("commandModePutChar: Unknown command: {}", mCommandBuffer);
    return {};
}

std::optional<CommandManager::GenCommand> CommandManager::commandModePutChar(CharCode const & c)
{
    if (c == '\n')
    {
        auto commandBufferCommand = executeCommandBuffer();
        if (!commandBufferCommand.has_value())
        {
            spdlog::debug("Invalid editor command: {}", mCommandBuffer);
            //TODO put in the status bar
        }
        mCommandBuffer.clear();
        mMode = Mode::Normal;
        return commandBufferCommand;
    }
    else
    {
        spdlog::debug("commandModePutChar: add character to command: {}({})", c, static_cast<unsigned char>(c));
        mCommandBuffer.push_back(c);
    }


    return {};
}



ParseTry<CommandManager::EditCommand> parseNormalEditOperatorCommand(CommandManager::CharVecRange const rng)
{
    if (rng.first == rng.second)
        return UndecidedParse{};

    auto c = *rng.first;
    switch (c)
    {
        case 'd':
        {
            return CommandManager::EditCommand{
                {"d", 1},
                CommandManager::EditCommandType::Delete, {}};
        }
    }
    return FailedParse{};
}
ParseTry<CommandManager::MoveCommand> parseNormalMoveCommand(CommandManager::CharVecRange const rng)
{
    auto c = *rng.first;
    switch (c)
    {
        case '/':
        {
            auto foundEndLine = std::find(std::next(rng.first), rng.second, '\n');
            if (foundEndLine != rng.second)
            {
                return CommandManager::MoveCommand{ {"h", 1},
                                                    CommandManager::MoveCommandType::MoveToSearch,
                                                    {std::next(rng.first),foundEndLine}};
            }
            else
                return UndecidedParse{};
        }
        case 'f':
        {
            auto nextCharIter = std::next(rng.first, 1);
            if (nextCharIter == rng.second)
                return UndecidedParse{};
            return CommandManager::MoveCommand{ {"h", 1},
                                                CommandManager::MoveCommandType::MoveToCharInLine,
                                                {nextCharIter,std::next(nextCharIter, 1)}};
        }
        case 'w':
        {
            return CommandManager::MoveCommand{
                    {"0", 1}, CommandManager::MoveCommandType::MoveWordFwd, ""};
        }
        case 'b':
        {
            return CommandManager::MoveCommand{
                    {"0", 1}, CommandManager::MoveCommandType::MoveWordBack, ""};
        }
        case '0':
        {
            return CommandManager::MoveCommand{
                    {"0", 1}, CommandManager::MoveCommandType::MoveToBeginLine, ""};
        }
        case '$':
        {
            return CommandManager::MoveCommand{
                    {"0", 1}, CommandManager::MoveCommandType::MoveToEndLine, ""};
        }

        case 'h':
        {
            return CommandManager::MoveCommand{ {"h", 1},
                                                CommandManager::MoveCommandType::MoveLeft, ""};
        }
        case 'l':
        {
            return CommandManager::MoveCommand{ {"l", 1},
                                                CommandManager::MoveCommandType::MoveRight, ""};
        }
        case 'j':
        {
            return CommandManager::MoveCommand{ {"j", 1},
                                                CommandManager::MoveCommandType::MoveDown, ""};
            break;
        }
        case 'k':
        {
            return CommandManager::MoveCommand{ {"k", 1},
                                                CommandManager::MoveCommandType::MoveUp, ""};
            break;
        }
        default:
        {
            spdlog::debug("normalModePutChar: unknown character: {}({})", c, static_cast<unsigned char>(c));
        }
    }
    return FailedParse{};
}

CommandManager::CharVecRange getCountRange(CommandManager::CharVecRange const rng)
{
    constexpr auto isFirstDigit = [](auto c){return c>='1' && c <= '9';};
    if (rng.first == rng.second || !isFirstDigit(*rng.first))
        return {rng.first,rng.first};

    auto endCountIter = std::find_if_not(std::next(rng.first), rng.second, isdigit);
    return {rng.first, endCountIter};
}


bool editOperNeedsMove(CommandManager::EditCommand const &editCmd)
{
    return (editCmd.mCommandType == CommandManager::EditCommandType::Delete);
}

ParseTry<CommandManager::MoveCommand> parseNormalMoveCommandWithRep(CommandManager::CharVecRange const rng)
{
    auto countRange = getCountRange(rng);
    if (countRange.second == rng.second)
        return UndecidedParse{};

    auto moveCommandParse = parseNormalMoveCommand({countRange.second, rng.second});
    if (isValue(moveCommandParse))
    {
        CommandManager::MoveCommand& theMoveCmd = std::get<CommandManager::MoveCommand>(moveCommandParse);
        theMoveCmd.mRepetition = parseCount(countRange);
        return theMoveCmd;
    }
    else
    {
        if (std::holds_alternative<FailedParse>(moveCommandParse))
            return FailedParse{};
        else
            return UndecidedParse{};
    }
}

ParseTry<CommandManager::GenCommand> parseNormalEditCommandWithRe(CommandManager::CharVecRange const rng, std::optional<CommandManager::EditCommand> const &lastEditCommand)
{
    auto countRange = getCountRange(rng);
    if (countRange.second == rng.second)
        return UndecidedParse{};

    if (*countRange.second == '.' && lastEditCommand)
    {
        auto repeatedCmd = *lastEditCommand;
        if (countRange.first!= countRange.second)
            repeatedCmd.mRepetition = parseCount(countRange);
        return repeatedCmd;
    }
    auto editOperatorCmd = parseNormalEditOperatorCommand({countRange.second, rng.second});
    if (std::holds_alternative<UndecidedParse>(editOperatorCmd))
        return UndecidedParse{};
    if (isValue(editOperatorCmd))
    {
        auto & curEdit = std::get<CommandManager::EditCommand>(editOperatorCmd);
        auto afterOperatorIter = std::next(countRange.second, curEdit.mCommandString.length());

        bool needsMove = editOperNeedsMove(curEdit);
        if (needsMove)
        {
            if (afterOperatorIter == rng.second)
                return UndecidedParse{};

            // Is it plain operator
            if (*afterOperatorIter == curEdit.mCommandString.back())
            {
                curEdit.mCommandString.push_back(*afterOperatorIter);
            }
            else // find move to apply
            {
                auto moveCmd = parseNormalMoveCommandWithRep({afterOperatorIter,rng.second});
                if (isValue(moveCmd))
                {
                    curEdit.mApplyTo = std::get<CommandManager::MoveCommand>(moveCmd);
                }
                else
                {
                    return convParse<CommandManager::GenCommand>(moveCmd);
                }
            }
        }
        curEdit.mRepetition = parseCount(countRange);
        return curEdit;
    }
    return FailedParse{};
}

ParseTry<CommandManager::GenCommand> parseNormalCommand(CommandManager::CharVecRange const rng, std::optional<CommandManager::EditCommand> const &lastEditCommand)
{
    auto retCmd = parseNormalMoveCommandWithRep(rng);
    if (!std::holds_alternative<FailedParse>(retCmd))
        return convParse<CommandManager::GenCommand>(retCmd);

    return parseNormalEditCommandWithRe(rng, lastEditCommand);
}

std::optional<CommandManager::GenCommand> CommandManager::normalModePutChar(CharCode const & c)
{
    if (mCommandBuffer.empty() && c == ':')
    {
        spdlog::debug("normalModePutChar: switch to command mode");
        mCommandBuffer.clear();
        mMode = Mode::Command;
        return {};
    }
    else
    {
        mCommandBuffer.push_back(c);
        auto wouldBeCommand = parseNormalCommand({mCommandBuffer.begin(), mCommandBuffer.end()}, mEditLastCommand);

        spdlog::debug("normalModePutChar: {}({})", c, (char) c);
        if (!mCommandBuffer.empty())
            spdlog::debug("Command buffer : {}", mCommandBuffer);
        spdlog::debug("Ret Cmd : {}", wouldBeCommand);

        if (std::holds_alternative<FailedParse>(wouldBeCommand) )
        {
            spdlog::debug("Bad Command: clear.");
            mCommandBuffer.clear();
            return {};
        }
        else if (std::holds_alternative<GenCommand>(wouldBeCommand) )
        {
            mCommandBuffer.clear();
            return std::get<GenCommand>(wouldBeCommand);
        }
        else
        {
            return {};
        }

        // Full op
        // ([1-9][0-9]*)?
        // ((dd)|(yy)) others

        /// op and move
        // ([1-9][0-9]*)?
        // ((d)|(y)) others
        // ([1-9][0-9]*)?
        // <Move>

        /// Move
        // ([1-9][0-9]*)?
        // hjkl | f.


    }
}

std::optional<CommandManager::GenCommand> CommandManager::escapeKey()
{
    spdlog::debug("CommandManager::escapeKey");
    mCommandBuffer.clear();
    mMode = Mode::Normal;

    return {};

}

std::optional<CommandManager::GenCommand> CommandManager::rightArrow()
{
    if (mMode == Mode::Normal)
        return putChar('l');
    return {};
}

std::optional<CommandManager::GenCommand> CommandManager::leftArrow()
{
    if (mMode == Mode::Normal)
        return putChar('h');
    return {};
}

std::optional<CommandManager::GenCommand> CommandManager::upArrow()
{
    if (mMode == Mode::Normal)
        return putChar('k');
    return {};
}

std::optional<CommandManager::GenCommand> CommandManager::downArrow()
{
    if (mMode == Mode::Normal)
        return putChar('j');
    return {};
}

ScreenLine CommandManager::getCommandLine() const
{
    ScreenLine commandLine{};
    if (mMode==Mode::Command)
    {
        commandLine.mStr += ':';
        for (auto c : mCommandBuffer)
            commandLine.mStr += static_cast<unsigned char>(c);
    }
    return commandLine;

}

void CommandManager::updateLastEditCommand(CommandManager::ReturnedCommand const &lastCommand)
{
    if (lastCommand && std::holds_alternative<EditCommand>(*lastCommand))
        mEditLastCommand = std::get<EditCommand>(*lastCommand);
}
