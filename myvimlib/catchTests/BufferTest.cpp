//
// Created by Jean-Simon Lapointe on 2020-09-05.
//

#define CATCH_CONFIG_ENABLE_ALL_STRINGMAKERS
#include "catch2/catch.hpp"
#include "TestUtil.h"

#include "../Buffer.h"

#include "spdlog/spdlog.h"

#include <filesystem>
#include <utility>
#include <iostream>
#include <fstream>

using namespace Catch::Matchers;
using namespace TestUtil;



TEST_CASE( "Empty Buffers", "[buffer]" ) {
    Buffer emptyBuffer;
    REQUIRE( !emptyBuffer.hasPath() );
    REQUIRE( !emptyBuffer.load() );
    REQUIRE( !emptyBuffer.save() );
    REQUIRE( emptyBuffer.nbLine() == 0 );
    REQUIRE_THAT( emptyBuffer.getContent(), Equals("") );
}

TEST_CASE( "Non Empty Buffers", "[buffer]" ) {
    auto myDir = createTmpDir();
    std::string const content{"This is the content"};
    auto myFile = myDir / "myFile.txt";
    createFile(myFile, content);
    Buffer myBuffer{myFile};

    REQUIRE( myBuffer.hasPath() );
    REQUIRE( myBuffer.nbLine() == 1 );
    REQUIRE_THAT( myBuffer.getContent(), Equals(content) );

    std::string const newContent{"This is the New and improved content"};
    SECTION("Reload")
    {
        createFile(myFile, newContent);

        REQUIRE_THAT( myBuffer.getContent(), Equals(content) );
        REQUIRE( myBuffer.load() );
        REQUIRE_THAT( myBuffer.getContent(), Equals(newContent) );
    }

    SECTION("Change")
    {
        REQUIRE(!myBuffer.isOutdated());
        myBuffer.setContent(newContent);
        REQUIRE(myBuffer.isOutdated());
        REQUIRE_THAT( myBuffer.getContent(), Equals(newContent) );
        REQUIRE_THAT( readFile(myFile), Equals(content) );
        REQUIRE( myBuffer.save() );
        REQUIRE(!myBuffer.isOutdated());
        REQUIRE_THAT( myBuffer.getContent(), Equals(newContent) );
        REQUIRE_THAT( readFile(myFile), Equals(newContent) );
    }

    SECTION("GetLine")
    {
        REQUIRE_THAT( myBuffer.getLine(0), Equals(content) );
        REQUIRE_THAT( myBuffer.getLine(1), Equals("") );
    }

}

TEST_CASE( "Multi line Buffers", "[buffer]" )
{
    auto myDir = createTmpDir();

    std::string const content =
            "This is the content Line 1\n"
            "This is the content Line 2\n"
            "This is the content Line 3\n"
            ;
    auto myFile = myDir / "myFile.txt";
    createFile(myFile, content);

    Buffer myBuffer{myFile};

    SECTION("GetLine tests")
    {
        REQUIRE(myBuffer.getLine(99) == "");
        REQUIRE(Buffer{}.getLine(0) == "");
        REQUIRE(Buffer{}.getLine(-1) == "");
    }

    SECTION("Misc offsets")
    {

        REQUIRE(myBuffer.hasPath());
        REQUIRE(myBuffer.nbLine() == 3);
        CHECK(myBuffer.getLineNumAndOffsetForBufferOffset(0) == std::make_pair(0,0)); // T
        CHECK(myBuffer.getLineNumAndOffsetForBufferOffset(26) == std::make_pair(0,26)); // \n
        CHECK(myBuffer.getLineNumAndOffsetForBufferOffset(27) == std::make_pair(1,0)); // T
        CHECK(myBuffer.getLineNumAndOffsetForBufferOffset(28) == std::make_pair(1,1)); // h

        CHECK(myBuffer.getLineNumAndOffsetForBufferOffset(52) == std::make_pair(1,25)); //
        CHECK(myBuffer.getLineNumAndOffsetForBufferOffset(53) == std::make_pair(1,26)); // \n
        CHECK(myBuffer.getLineNumAndOffsetForBufferOffset(54) == std::make_pair(2,0)); // T

        CHECK(myBuffer.getLineNumAndOffsetForBufferOffset(81) == std::make_pair(3,0)); // T

        REQUIRE(myBuffer.getBufferOffsetFromStartOfLine(0) == 0);
        REQUIRE(myBuffer.getBufferOffsetFromStartOfLine(1) == 27);
        REQUIRE(myBuffer.getBufferOffsetFromStartOfLine(2) == 54);
        REQUIRE(myBuffer.getBufferOffsetFromStartOfLine(3) == 81);
    }

    SECTION("Regex search")
    {
        std::regex lineRegex{"Line"};
        REQUIRE(*myBuffer.match(lineRegex) == 20);
        REQUIRE(*myBuffer.match(lineRegex, 10) == 20);
        REQUIRE(*myBuffer.match(lineRegex, 20) == 20);
        REQUIRE(*myBuffer.match(lineRegex, 21) == 47);
        REQUIRE(!myBuffer.match(lineRegex, 77).has_value());

        REQUIRE(!myBuffer.match(std::regex{"Patate"}).has_value());
        REQUIRE(!myBuffer.match(lineRegex, 100).has_value());
    }

    SECTION("Delete buffer chunk")
    {
        auto posStart = GENERATE(0, 10, 27, 38, 53, 54, 55, 56, 57, 58, 59, 76, 77, 78, 79, 80, 81, 82);
        auto posEnd   = GENERATE(0, 10, 27, 38, 53, 54, 55, 56, 57, 58, 59, 76, 77, 78, 79, 80, 81, 82);
        WHEN("Deleting From " << posStart << " To " << posEnd)
        {
            if (posStart > posEnd)
            {
                CHECK_THROWS(myBuffer.deleteChunk(posStart, posEnd));
                CHECK(!myBuffer.isOutdated());
            }
            else if (posEnd >= content.length())
            {
                CHECK_THROWS(myBuffer.deleteChunk(posStart, posEnd));
                CHECK(!myBuffer.isOutdated());
            }
            else
            {
                myBuffer.deleteChunk(posStart, posEnd);
                std::string expectedContent{content};
                expectedContent.erase(posStart, posEnd-posStart+1);
                CHECK(myBuffer.getContent() == expectedContent);
                CHECK(myBuffer.getContent() != content);
                CHECK(myBuffer.isOutdated());
            }
        }

    }

}
