//
// Created by Jean-Simon Lapointe on 2020-09-06.
//

#include "TestUtil.h"

#include <filesystem>
#include <iostream>
#include <fstream>


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
std::filesystem::path TestUtil::createTmpDir()
{
    std::filesystem::path pathName{std::tmpnam(nullptr)};
    std::filesystem::create_directory(pathName);
    return pathName;
}
#pragma GCC diagnostic pop

void TestUtil::createFile(std::filesystem::path const & filePath, std::string const & content /*= {}*/)
{
    std::ofstream str{filePath};
    if (!content.empty())
        str << content;
    str.close();
}

std::string TestUtil::readFile(std::filesystem::path const & filePath)
{
    std::ifstream str{filePath};
    return {(std::istreambuf_iterator<char>(str)),
            std::istreambuf_iterator<char>()};
}