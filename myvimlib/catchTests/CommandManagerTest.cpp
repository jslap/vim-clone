//
// Created by Jean-Simon Lapointe on 2020-09-13.
//
#include "catch2/catch.hpp"
#include "TestUtil.h"

#include "../CommandManager.h"

using namespace TestUtil;
using namespace Catch::Matchers;

class CommandIsQuit : public Catch::MatcherBase<CommandManager::ReturnedCommand> {
public:
    // Performs the test for this matcher
    bool match( CommandManager::ReturnedCommand const& cmd ) const override {
        return cmd.has_value() &&
               std::get<CommandManager::ApplicationCommand>(*cmd).mCommandType == CommandManager::AppCommandType::Quit;
    }

    virtual std::string describe() const override {
        return "Is a Application Quit command";
    }
};

class CommandIsReload : public Catch::MatcherBase<CommandManager::ReturnedCommand> {
public:
    // Performs the test for this matcher
    bool match( CommandManager::ReturnedCommand const& cmd ) const override {
        return cmd.has_value() &&
               std::get<CommandManager::ApplicationCommand>(*cmd).mCommandType == CommandManager::AppCommandType::Reload;
    }

    virtual std::string describe() const override {
        return "Is a Application Reload command";
    }
};

class CommandIsReloadHard : public Catch::MatcherBase<CommandManager::ReturnedCommand> {
public:
    // Performs the test for this matcher
    bool match( CommandManager::ReturnedCommand const& cmd ) const override {
        return cmd.has_value() &&
               std::get<CommandManager::ApplicationCommand>(*cmd).mCommandType == CommandManager::AppCommandType::ReloadHard;
    }

    virtual std::string describe() const override {
        return "Is a Application Reload Hard command";
    }
};

class CommandIsSave : public Catch::MatcherBase<CommandManager::ReturnedCommand> {
public:
    // Performs the test for this matcher
    bool match( CommandManager::ReturnedCommand const& cmd ) const override {
        return cmd.has_value() &&
               std::get<CommandManager::ApplicationCommand>(*cmd).mCommandType == CommandManager::AppCommandType::Save;
    }

    virtual std::string describe() const override {
        return "Is a Application Save command";
    }
};

class CommandIsSaveAndQuit : public Catch::MatcherBase<CommandManager::ReturnedCommand> {
public:
    // Performs the test for this matcher
    bool match( CommandManager::ReturnedCommand const& cmd ) const override {
        return cmd.has_value() &&
               std::get<CommandManager::ApplicationCommand>(*cmd).mCommandType == CommandManager::AppCommandType::SaveAndQuit;
    }

    virtual std::string describe() const override {
        return "Is a Application SaveAndQuit command";
    }
};

class CommandIsQuitHard : public Catch::MatcherBase<CommandManager::ReturnedCommand> {
public:
    // Performs the test for this matcher
    bool match( CommandManager::ReturnedCommand const& cmd ) const override {
        return cmd.has_value() &&
               std::get<CommandManager::ApplicationCommand>(*cmd).mCommandType == CommandManager::AppCommandType::QuitHard;
    }

    virtual std::string describe() const override {
        return "Is a Application QuitHard command";
    }
};

class CommandIsNull : public Catch::MatcherBase<CommandManager::ReturnedCommand> {
public:
    // Performs the test for this matcher
    bool match( CommandManager::ReturnedCommand const& cmd ) const override {
        return !cmd.has_value();
    }

    virtual std::string describe() const override {
        return "Is a Null command";
    }
};

TEST_CASE( "CommandManager", "[command]" )
{
    CommandManager cmdMan;

    SECTION("Default State")
    {
        REQUIRE(cmdMan.getMode() == Mode::Normal);
    }
    SECTION("Editing...")
    {
        SECTION("Delete")
        {
            WHEN("Delete plain")
            {
                REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());

                auto cmd = cmdMan.putChar('d');
                REQUIRE(cmd.has_value());
                REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
                REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                        CommandManager::EditCommandType::Delete);
                REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 1);
                auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
                REQUIRE(!moveCommandApply.has_value());
            }
            if(0) //WHEN("Delete left")
            {
                REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());

                auto cmd = cmdMan.putChar('l');
                REQUIRE(cmd.has_value());
                REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
                REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                        CommandManager::EditCommandType::Delete);
                REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 1);
                auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
                REQUIRE(moveCommandApply.has_value());
                REQUIRE(moveCommandApply->mCommandType ==
                        CommandManager::MoveCommandType::MoveRight);
                REQUIRE(moveCommandApply->mRepetition == 1);
            }

        }

    }
    SECTION("Move around")
    {
        SECTION("Word moves")
        {
            auto wordFwdCmd = cmdMan.putChar('w');
            REQUIRE(wordFwdCmd.has_value());
            REQUIRE(std::get<CommandManager::MoveCommand>(*wordFwdCmd).mCommandType ==
                    CommandManager::MoveCommandType::MoveWordFwd);

            auto wordBackCmd = cmdMan.putChar('b');
            REQUIRE(wordBackCmd.has_value());
            REQUIRE(std::get<CommandManager::MoveCommand>(*wordBackCmd).mCommandType ==
                    CommandManager::MoveCommandType::MoveWordBack);
        }
        SECTION("Arrows keys")
        {
            auto leftCmd = cmdMan.leftArrow();
            REQUIRE(leftCmd.has_value());
            REQUIRE(std::get<CommandManager::MoveCommand>(*leftCmd).mCommandType ==
                            CommandManager::MoveCommandType::MoveLeft);

            auto rightCmd = cmdMan.rightArrow();
            REQUIRE(rightCmd.has_value());
            REQUIRE(std::get<CommandManager::MoveCommand>(*rightCmd).mCommandType ==
                    CommandManager::MoveCommandType::MoveRight);

            auto upCmd = cmdMan.upArrow();
            REQUIRE(upCmd.has_value());
            REQUIRE(std::get<CommandManager::MoveCommand>(*upCmd).mCommandType ==
                    CommandManager::MoveCommandType::MoveUp);

            auto downCmd = cmdMan.downArrow();
            REQUIRE(downCmd.has_value());
            REQUIRE(std::get<CommandManager::MoveCommand>(*downCmd).mCommandType ==
                    CommandManager::MoveCommandType::MoveDown);
        }

        SECTION("Char Arrow keys")
        {
            auto cmdPair = GENERATE(table<CharCode , CommandManager::MoveCommandType>({
                                                                                              {'l', CommandManager::MoveCommandType::MoveRight},
                                                                                              {'h', CommandManager::MoveCommandType::MoveLeft},
                                                                                              {'j', CommandManager::MoveCommandType::MoveDown},
                                                                                              {'k', CommandManager::MoveCommandType::MoveUp}
                                                                                      }));
            WHEN("character entered is " << std::get<0>(cmdPair))
            {
                auto cmd = cmdMan.putChar(std::get<0>(cmdPair));
                THEN("I expect command to be " << (int)std::get<1>(cmdPair))
                {
                    REQUIRE(cmd.has_value());
                    REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mCommandType ==
                            std::get<1>(cmdPair));
                }
            }
        }

        SECTION("Mode changes")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);

            REQUIRE(!cmdMan.putChar(':'));
            REQUIRE(cmdMan.getMode() == Mode::Command);

            REQUIRE(!cmdMan.putChar('q'));
            REQUIRE(cmdMan.getMode() == Mode::Command);

            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsQuit());
            REQUIRE(cmdMan.getMode() == Mode::Normal);
        }

        SECTION("Mode change test")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            REQUIRE(cmdMan.getCommandLine().mStr == ":");
            REQUIRE(cmdMan.getMode() == Mode::Command);
            cmdMan.escapeKey();
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            REQUIRE(cmdMan.getCommandLine().mStr == "");
        }
        SECTION("Command Save")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            cmdMan.putChar('w');
            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsSave());
        }
        SECTION("Command Save And Quit")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            cmdMan.putChar('w');
            cmdMan.putChar('q');
            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsSaveAndQuit());
        }
        SECTION("Command Quit Hard")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            cmdMan.putChar('q');
            cmdMan.putChar('!');
            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsQuitHard());
        }
        SECTION("Command Reload Hard")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            cmdMan.putChar('e');
            cmdMan.putChar('!');
            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsReloadHard());
        }
        SECTION("Command Reload")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            cmdMan.putChar('e');
            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsReload());
        }
        SECTION("Command Not valid")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            cmdMan.putChar('r');
            cmdMan.putChar('w');
            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsNull());

            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            cmdMan.putChar('w');
            cmdMan.putChar('w');
            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsNull());
        }

        SECTION("Mode change test")
        {
            REQUIRE(cmdMan.getMode() == Mode::Normal);
            cmdMan.putChar(':');
            cmdMan.putChar('q');
            REQUIRE(cmdMan.getCommandLine().mStr == ":q");
            REQUIRE(cmdMan.getMode() == Mode::Command);
            REQUIRE_THAT(cmdMan.escapeKey(), CommandIsNull());
            REQUIRE(cmdMan.getCommandLine().mStr == "");
            REQUIRE(cmdMan.getMode() == Mode::Normal);

            cmdMan.putChar(':');
            cmdMan.putChar('q');
            REQUIRE_THAT(cmdMan.putChar('\n'), CommandIsQuit());
            REQUIRE(cmdMan.getCommandLine().mStr == "");
        }
    }

    SECTION("Test Command repetition")
    {
        auto nbRepetition = GENERATE(1, 2, 3, 4, 10, 11, 20, 133);
        for (auto digit : std::to_string(nbRepetition))
        {
            REQUIRE_THAT(cmdMan.putChar(digit), CommandIsNull());
        }
        auto cmd = cmdMan.putChar('l');
        REQUIRE(cmd.has_value());
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mCommandType ==
                CommandManager::MoveCommandType::MoveRight);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mRepetition == nbRepetition);
    }

    SECTION("Check non valid inputs")
    {
        REQUIRE_THAT(cmdMan.putChar('o'), CommandIsNull());

        auto cmd = cmdMan.putChar('l');
        REQUIRE(cmd.has_value());
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mCommandType ==
                CommandManager::MoveCommandType::MoveRight);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mRepetition == 1);
    }

    SECTION("Check move to next char")
    {
        REQUIRE_THAT(cmdMan.putChar('f'), CommandIsNull());

        auto cmd = cmdMan.putChar('s');
        REQUIRE(cmd.has_value());
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mCommandType ==
                CommandManager::MoveCommandType::MoveToCharInLine);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mRepetition == 1);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mArgument == "s");
    }

    SECTION("Move to begining of line")
    {
        auto cmd = cmdMan.putChar('0');
        REQUIRE(cmd.has_value());
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mCommandType ==
                CommandManager::MoveCommandType::MoveToBeginLine);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mRepetition == 1);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mArgument == "");
    }

    SECTION("Move to end of line")
    {
        auto cmd = cmdMan.putChar('$');
        REQUIRE(cmd.has_value());
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mCommandType ==
                CommandManager::MoveCommandType::MoveToEndLine);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mRepetition == 1);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mArgument == "");
    }

    SECTION("Move to search")
    {
        REQUIRE_THAT(cmdMan.putChar('/'), CommandIsNull());
        REQUIRE_THAT(cmdMan.putChar('L'), CommandIsNull());
        REQUIRE_THAT(cmdMan.putChar('i'), CommandIsNull());
        REQUIRE_THAT(cmdMan.putChar('n'), CommandIsNull());
        REQUIRE_THAT(cmdMan.putChar('e'), CommandIsNull());

        auto cmd = cmdMan.putChar('\n');
        REQUIRE(cmd.has_value());
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mCommandType ==
                CommandManager::MoveCommandType::MoveToSearch);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mRepetition == 1);
        REQUIRE(std::get<CommandManager::MoveCommand>(*cmd).mArgument == "Line");
    }

    SECTION("edit with move")
    {
        SECTION("simple move")
        {
            REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());
            auto cmd = cmdMan.putChar('l');
            REQUIRE(cmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 1);
            auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
            REQUIRE(moveCommandApply.has_value());
            REQUIRE(moveCommandApply->mCommandType == CommandManager::MoveCommandType::MoveRight);
            REQUIRE(moveCommandApply->mRepetition == 1);
        }

        SECTION("move with rep")
        {
            REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());
            REQUIRE_THAT(cmdMan.putChar('2'), CommandIsNull());
            auto cmd = cmdMan.putChar('l');
            REQUIRE(cmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 1);
            auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
            REQUIRE(moveCommandApply.has_value());
            REQUIRE(moveCommandApply->mCommandType == CommandManager::MoveCommandType::MoveRight);
            REQUIRE(moveCommandApply->mRepetition == 2);
        }
        SECTION("oper with rep and move with rep")
        {
            REQUIRE_THAT(cmdMan.putChar('2'), CommandIsNull());
            REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());
            REQUIRE_THAT(cmdMan.putChar('2'), CommandIsNull());
            auto cmd = cmdMan.putChar('l');
            REQUIRE(cmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 2);
            auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
            REQUIRE(moveCommandApply.has_value());
            REQUIRE(moveCommandApply->mCommandType == CommandManager::MoveCommandType::MoveRight);
            REQUIRE(moveCommandApply->mRepetition == 2);
        }
    }
    SECTION("Repeat last command")
    {
        SECTION("No prev cmd")
        {
            REQUIRE_THAT(cmdMan.putChar('.'), CommandIsNull());
            cmdMan.putChar('j');
            REQUIRE_THAT(cmdMan.putChar('.'), CommandIsNull());
        }
        SECTION("with prev cmd no rep")
        {
            REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());
            auto cmd = cmdMan.putChar('d');
            REQUIRE(cmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 1);
            auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
            REQUIRE(!moveCommandApply.has_value());

            auto repCmd = cmdMan.putChar('.');
            REQUIRE(repCmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*repCmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*repCmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*repCmd).mRepetition == 1);
            auto repMoveCommandApply = std::get<CommandManager::EditCommand>(*repCmd).mApplyTo;
            REQUIRE(!repMoveCommandApply.has_value());
        }
        SECTION("with prev cmd that has repetition")
        {
            REQUIRE_THAT(cmdMan.putChar('2'), CommandIsNull());
            REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());
            auto cmd = cmdMan.putChar('d');
            REQUIRE(cmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 2);
            auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
            REQUIRE(!moveCommandApply.has_value());

            auto repCmd = cmdMan.putChar('.');
            REQUIRE(repCmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*repCmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*repCmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*repCmd).mRepetition == 2);
            auto repMoveCommandApply = std::get<CommandManager::EditCommand>(*repCmd).mApplyTo;
            REQUIRE(!repMoveCommandApply.has_value());
        }
        SECTION("with repetition prev cmd no rep")
        {
            REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());
            auto cmd = cmdMan.putChar('d');
            REQUIRE(cmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 1);
            auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
            REQUIRE(!moveCommandApply.has_value());

            REQUIRE_THAT(cmdMan.putChar('3'), CommandIsNull());
            auto repCmd = cmdMan.putChar('.');
            REQUIRE(repCmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*repCmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*repCmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*repCmd).mRepetition == 3);
            auto repMoveCommandApply = std::get<CommandManager::EditCommand>(*repCmd).mApplyTo;
            REQUIRE(!repMoveCommandApply.has_value());
        }
        SECTION("with override repetition prev cmd")
        {
            REQUIRE_THAT(cmdMan.putChar('2'), CommandIsNull());
            REQUIRE_THAT(cmdMan.putChar('d'), CommandIsNull());
            auto cmd = cmdMan.putChar('d');
            REQUIRE(cmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*cmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*cmd).mRepetition == 2);
            auto moveCommandApply = std::get<CommandManager::EditCommand>(*cmd).mApplyTo;
            REQUIRE(!moveCommandApply.has_value());

            REQUIRE_THAT(cmdMan.putChar('3'), CommandIsNull());
            auto repCmd = cmdMan.putChar('.');
            REQUIRE(repCmd.has_value());
            REQUIRE(std::holds_alternative<CommandManager::EditCommand>(*repCmd));
            REQUIRE(std::get<CommandManager::EditCommand>(*repCmd).mCommandType ==
                    CommandManager::EditCommandType::Delete);
            REQUIRE(std::get<CommandManager::EditCommand>(*repCmd).mRepetition == 3);
            auto repMoveCommandApply = std::get<CommandManager::EditCommand>(*repCmd).mApplyTo;
            REQUIRE(!repMoveCommandApply.has_value());
        }
    }

}
