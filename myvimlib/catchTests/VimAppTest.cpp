//
// Created by Jean-Simon Lapointe on 2020-09-06.
//

#include "catch2/catch.hpp"
#include "TestUtil.h"

#include "../VimApp.h"

#include <iostream>
#include <fstream>

using namespace TestUtil;
using namespace Catch::Matchers;

class AppTestFixture
{
public:
    static void checkInit()
    {
        if (!isInit)
        {
            isInit = true;
            myStaticFileDir = createTmpDir();

            // small file
            smallFilePath = myStaticFileDir / "myFile.txt";
            smallFileBufferNbLine = 4;
            smallFileBufferLineSize = 27;
            {
                std::string const content =
                        "This is the content Line 1\n"
                        "This is the content Line 2\n"
                        "This is the content Line 3\n";
                createFile(smallFilePath, content);
            }
            smallFileContent = readFile(smallFilePath);
        }

        // big file
        bigFileFullPath = myStaticFileDir / "myBigFile.txt";
        bigFileBufferNbLine = 20;
        bigFileBufferLineSize = 31;
        bigFileLineHeaderSize = 4;
        {
            std::string content;
            for (int i = 0; i < bigFileBufferNbLine; i++)
                content += fmt::format("This is the content Line {:5}\n", i + 1);
            createFile(bigFileFullPath, content);
        }
        bigFileContent = readFile(bigFileFullPath);
    }

    AppTestFixture()
    {
        checkInit();
    }

    class WorkingCopy
    {
    public:
        WorkingCopy(std::filesystem::path const & src): mSrcPath{src} ,mCopyPath{src.parent_path()/(src.filename().string() + "copy")}
        {
            createFile(mCopyPath, readFile(mSrcPath));
        }
        ~WorkingCopy() {std::filesystem::remove(mCopyPath);}
        std::filesystem::path const & getPath() {return mCopyPath;}
        std::filesystem::path const & operator()() {return mCopyPath;}

    private:
        std::filesystem::path mSrcPath;
        std::filesystem::path mCopyPath;
    };

    static bool isInit;
    static std::filesystem::path myStaticFileDir;

    static std::filesystem::path smallFilePath;
    static int smallFileBufferNbLine;
    static int smallFileBufferLineSize;
    static std::string smallFileContent;


    static std::filesystem::path bigFileFullPath;
    static int bigFileBufferNbLine;
    static int bigFileBufferLineSize;
    static int bigFileLineHeaderSize;
    static std::string bigFileContent;
};

bool AppTestFixture::isInit{false};
std::filesystem::path AppTestFixture::myStaticFileDir{};

std::filesystem::path AppTestFixture::smallFilePath{};
int AppTestFixture::smallFileBufferNbLine{};
int AppTestFixture::smallFileBufferLineSize{};
std::string AppTestFixture::smallFileContent{};


std::filesystem::path AppTestFixture::bigFileFullPath{};
int AppTestFixture::bigFileBufferNbLine{};
int AppTestFixture::bigFileBufferLineSize{};
int AppTestFixture::bigFileLineHeaderSize{};
std::string AppTestFixture::bigFileContent{};


TEST_CASE_METHOD(AppTestFixture, "Empty App", "[app]")
{
    int nbLine = 10;
    VimApp theApp{40, nbLine};
    REQUIRE(theApp.getNumCol() == 40);
    REQUIRE(theApp.getNumLine() == nbLine);
    REQUIRE(theApp.getCursorPos() == VimApp::ScreenPos{3, 0});
    REQUIRE(theApp.getScreen().mLines.size() == nbLine);
    for (int i = 0; i < nbLine; i++)
        REQUIRE_THAT(theApp.getScreen().mLines[i].mStr, Equals(""));

    SECTION("Check Should quit func")
    {
        REQUIRE(!theApp.appIsDone());
        theApp.quit();
        REQUIRE(theApp.appIsDone());
    }
    SECTION("Check Should quit keys")
    {
        REQUIRE(!theApp.appIsDone());
        theApp.putChar(':');
        REQUIRE(!theApp.appIsDone());
        theApp.putChar('q');
        REQUIRE(!theApp.appIsDone());
        theApp.putChar('\n');
        REQUIRE(theApp.appIsDone());
    }

    SECTION("Mode change test")
    {
        REQUIRE(theApp.getMode() == Mode::Normal);
        theApp.putChar(':');
        REQUIRE(theApp.getMode() == Mode::Command);
        theApp.escapeKey();
        REQUIRE(theApp.getMode() == Mode::Normal);
    }
    SECTION("Mode change test")
    {
        REQUIRE(theApp.getMode() == Mode::Normal);
        theApp.putChar(':');
        theApp.putChar('q');
        REQUIRE(theApp.getMode() == Mode::Command);
        theApp.escapeKey();
        REQUIRE(theApp.getMode() == Mode::Normal);
        REQUIRE(!theApp.appIsDone());

        theApp.putChar(':');
        theApp.putChar('q');
        theApp.putChar('\n');
        REQUIRE(theApp.appIsDone());
    }
}

TEST_CASE_METHOD(AppTestFixture, "Multi Line App", "[app]")
{
    int nbLine = 10;
    VimApp theApp{40, nbLine, smallFilePath};

    SECTION("Check cursor from buffer offset With ")
    {
        int maxBufPos = smallFileContent.length() - 2;
        REQUIRE_THROWS(theApp.setCursorBufferPosAndRefresh(maxBufPos + 1));
        for (int bufPos = 0; bufPos <= maxBufPos; bufPos++)
        {
            INFO("Buffer position is " << bufPos);
            theApp.setCursorBufferPosAndRefresh(bufPos);
            int curLine = bufPos / 27;
            int offsetInLine = bufPos % 27;
            REQUIRE(theApp.getCursorPos() == VimApp::ScreenPos{3 + offsetInLine, curLine});
            REQUIRE(theApp.getLineOfCursor() == curLine);
        }
    }

    SECTION("Check orig screen")
    {
        REQUIRE(theApp.getNumCol() == 40);
        REQUIRE(theApp.getNumLine() == nbLine);
        REQUIRE(theApp.getCursorPos() == VimApp::ScreenPos{3, 0});
        REQUIRE(theApp.getLineOfCursor() == 0);
        REQUIRE(theApp.getScreen().mLines.size() == nbLine);
        REQUIRE_THAT(theApp.getScreen().mLines[0].mStr, Equals(" 1 This is the content Line 1"));
        REQUIRE_THAT(theApp.getScreen().mLines[1].mStr, Equals(" 2 This is the content Line 2"));
        REQUIRE_THAT(theApp.getScreen().mLines[2].mStr, Equals(" 3 This is the content Line 3"));
        for (int i = 3; i < nbLine; i++)
            REQUIRE_THAT(theApp.getScreen().mLines[i].mStr, Equals(""));
    }

    SECTION("Command Mode screen view")
    {
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Equals(""));
        theApp.putChar(':');
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Equals(":"));
        theApp.putChar('q');
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Equals(":q"));
        theApp.escapeKey();
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Equals(""));
    }

    SECTION("Move Cursor Right on screen")
    {
        int bufferLength = smallFileContent.length();
        auto nbRight = GENERATE_COPY(range((int) 1, (int) (bufferLength + 7)));
        SECTION("With n command")
        {
            CommandManager::MoveCommand cmdOneRight{{"", 1}, CommandManager::MoveCommandType::MoveRight};
            WHEN("Moving Right by " << nbRight)
            {
                for (int i = 0; i < nbRight; i++)
                    theApp.executeCommand(cmdOneRight);
                THEN("Buffer Pos should be moved")
                {
                    if (nbRight < smallFileContent.length() - (smallFileBufferNbLine - 1))
                        REQUIRE(theApp.getBufferPos() == nbRight + ((nbRight) / (smallFileBufferLineSize - 1)));
                    else
                        REQUIRE(theApp.getBufferPos() == bufferLength - 2);
                }
            }
        }

        SECTION("With 1 command and n repetition")
        {
            WHEN("Moving Right by " << nbRight)
            {
                CommandManager::MoveCommand cmdOneRight{{"", nbRight}, CommandManager::MoveCommandType::MoveRight};
                theApp.executeCommand(cmdOneRight);
                THEN("Buffer Pos should be moved")
                {
                    if (nbRight < smallFileContent.length() - (smallFileBufferNbLine - 1))
                        REQUIRE(theApp.getBufferPos() == nbRight + ((nbRight) / (smallFileBufferLineSize - 1)));
                    else
                        REQUIRE(theApp.getBufferPos() == bufferLength - 2);
                }
            }
        }
    }

    SECTION("Move Cursor Left on screen")
    {
        GIVEN("We are at the end of the buffer")
        {
            theApp.setCursorBufferPosAndRefresh(smallFileContent.length() - 2);
            //auto nbLeft = GENERATE_COPY(range((int) 1, (int) (smallFileContent.length() + 7)));
            auto nbLeft = GENERATE_COPY(1, 2, 3, 28, 29, 30, 32, 32,
                                        smallFileContent.length()-2,
                                        smallFileContent.length()-1,
                                        smallFileContent.length(),
                                        smallFileContent.length()+1,
                                        smallFileContent.length()+2);
            WHEN("We move left with many commands by " << nbLeft)
            {
                CommandManager::MoveCommand cmdOneLeft{{"", 1}, CommandManager::MoveCommandType::MoveLeft};
                for (int i = 0; i < nbLeft; i++)
                    theApp.executeCommand(cmdOneLeft);
                THEN("Buffer Pos should be moved")
                {
                    if (nbLeft < smallFileContent.length() - (smallFileBufferNbLine - 1))
                        REQUIRE(theApp.getBufferPos() == (smallFileContent.length() - 2) -
                                                         (nbLeft + ((nbLeft) / (smallFileBufferLineSize - 1))));
                    else
                        REQUIRE(theApp.getBufferPos() == 0);
                }

            }

            WHEN("We move left with 1 commands with repetition by " << nbLeft)
            {
                CommandManager::MoveCommand cmdOneLeft{{"", nbLeft}, CommandManager::MoveCommandType::MoveLeft};
                theApp.executeCommand(cmdOneLeft);
                THEN("Buffer Pos should be moved")
                {
                    if (nbLeft < smallFileContent.length() - (smallFileBufferNbLine - 1))
                        REQUIRE(theApp.getBufferPos() == (smallFileContent.length() - 2) -
                                                         (nbLeft + ((nbLeft) / (smallFileBufferLineSize - 1))));
                    else
                        REQUIRE(theApp.getBufferPos() == 0);
                }
            }
        }
    }


    SECTION("Move Cursor down on screen")
    {
        auto nbDown = GENERATE(range(1, 3));
        WHEN("Moving Down by " << nbDown)
        {
            for (int i = 0; i < nbDown; i++)
                theApp.putChar('j');
            THEN("Cursor Pos should be moved")
            {
                REQUIRE(theApp.getCursorPos() == VimApp::ScreenPos{3, nbDown});
                REQUIRE(theApp.getLineOfCursor() == nbDown);
            }
        }
    }

    SECTION("Move Cursor Down when already down on screen")
    {
        auto nbDown = GENERATE(range(1, 26));
        GIVEN("Already completely down")
        {
            for (int i = 0; i < 3; i++)
                theApp.putChar('j');
            WHEN("Moving Down by " << nbDown)
            {
                for (int i = 0; i < nbDown; i++)
                    theApp.putChar('j');
                THEN("Cursor Pos should be moved")
                {
                    REQUIRE(theApp.getCursorPos() == VimApp::ScreenPos{3, 2});
                    REQUIRE(theApp.getLineOfCursor() == 2);
                }
            }

        }
    }

    SECTION("Move Cursor Up on screen")
    {
        GIVEN("We are completely Down")
        {
            for (int i = 0; i < 3; i++)
                theApp.putChar('j');
            auto nbUp = GENERATE(range(1, 2));
            WHEN("Moving Up by " << nbUp)
            {
                for (int i = 0; i < nbUp; i++)
                    theApp.putChar('k');
                THEN("Cursor Pos should be moved")
                {
                    REQUIRE(theApp.getCursorPos() == VimApp::ScreenPos{3, 2 - nbUp});
                    REQUIRE(theApp.getLineOfCursor() == (2 - nbUp));
                }
            }
        }
    }

    SECTION("Move Cursor Up when already up on screen")
    {
        auto nbUp = GENERATE(range(1, 26));
        WHEN("Moving Left by " << nbUp)
        {
            for (int i = 0; i < nbUp; i++)
                theApp.putChar('k');
            THEN("Cursor Pos should be moved")
            {
                REQUIRE(theApp.getCursorPos() == VimApp::ScreenPos{3, 0});
                REQUIRE(theApp.getLineOfCursor() == 0);
            }
        }
    }
}

TEST_CASE_METHOD(AppTestFixture, "Buffer bigger than window App", "[app]")
{
    int appNbLine = 10;
    VimApp theApp{40, appNbLine, bigFileFullPath};
    int appNbLineForBuffer = appNbLine - 2;

    SECTION("Check Move to char in current line")
    {
        using repetPosPair = std::pair<int, int>;
        auto extent = GENERATE(table<int, int>({
                                                       repetPosPair{1, 2},
                                                       repetPosPair{2, 5},
                                                       repetPosPair{3, 21},
                                                       repetPosPair{4, 21},
                                                       repetPosPair{10, 21}
                                               }));
        WHEN("We repeat go to next char : " << std::get<0>(extent))
        {
            CommandManager::MoveCommand moveCmd{{"", std::get<0>(extent)}, CommandManager::MoveCommandType::MoveToCharInLine, "i"};
            theApp.executeCommand(moveCmd);
            REQUIRE(theApp.getBufferPos() == std::get<1>(extent));
        }
    }

    SECTION("Move Cursor to search")
    {
        auto nbRep = GENERATE(1,2,3,4,10);
        WHEN("Nb repetition is " << nbRep)
        {
            CommandManager::MoveCommand moveCmd{{"", nbRep}, CommandManager::MoveCommandType::MoveToSearch, "Line"};
            theApp.executeCommand(moveCmd);
            REQUIRE(theApp.getBufferPos() == (nbRep-1)*bigFileBufferLineSize + 20);
        }
    }

    SECTION("Check Move to Beginning of current line")
    {
        auto currentLine = GENERATE(0, 1, 2);
        WHEN("Current line is " << currentLine)
        {
            theApp.setCursorBufferPosAndRefresh(currentLine*bigFileBufferLineSize + bigFileBufferLineSize-4);
            auto nbRep = GENERATE(1,2,3,4,10);
            WHEN("We repeat go to begin : " << nbRep)
            {
                CommandManager::MoveCommand moveCmd{{"0", nbRep}, CommandManager::MoveCommandType::MoveToBeginLine, ""};
                theApp.executeCommand(moveCmd);
                REQUIRE(theApp.getBufferPos() == currentLine*bigFileBufferLineSize);
            }
        }
    }

    SECTION("Check Move to End of current line")
    {
        auto currentLine = GENERATE(0, 1, 2);
        WHEN("Current line is " << currentLine)
        {
            theApp.setCursorBufferPosAndRefresh(currentLine*bigFileBufferLineSize + 4);
            auto nbRep = GENERATE(1,2,3,4,10);
            WHEN("We repeat go to end : " << nbRep)
            {
                CommandManager::MoveCommand moveCmd{{"0", nbRep}, CommandManager::MoveCommandType::MoveToEndLine, ""};
                theApp.executeCommand(moveCmd);
                REQUIRE(theApp.getBufferPos() == currentLine*bigFileBufferLineSize + (bigFileBufferLineSize-2));
            }
        }
    }

    SECTION("Check cursor from buffer offset.")
    {
        int bigFileLength = bigFileContent.length();
        auto bufPos = GENERATE_REF(range(0, bigFileLength - 1));
        WHEN("Buffer position is " << bufPos)
        {
            theApp.setCursorBufferPosAndRefresh(bufPos);
            int curLine = bufPos / bigFileBufferLineSize;
            int offsetInLine = bufPos % bigFileBufferLineSize;
            if (curLine < appNbLine)
            {
                REQUIRE(theApp.getCursorPos() == VimApp::ScreenPos{bigFileLineHeaderSize + offsetInLine, curLine});
            } else
            {
                REQUIRE(theApp.getCursorPos() ==
                        VimApp::ScreenPos{bigFileLineHeaderSize + offsetInLine, appNbLine - 1});
            }
            REQUIRE(theApp.getLineOfCursor() == curLine);
        }

    }

    SECTION("Move Cursor Down")
    {
        auto nbDown = GENERATE_COPY(range(1, bigFileBufferNbLine + 4));
        SECTION("With n command")
        {
            CommandManager::MoveCommand cmdOneDown{{"", 1}, CommandManager::MoveCommandType::MoveDown};
            WHEN("Moving Down by " << nbDown)
            {
                for (int i = 0; i < nbDown; i++)
                    theApp.executeCommand(cmdOneDown);
                theApp.refreshScreen();
                THEN("Cursor Pos should be moved")
                {
                    REQUIRE(theApp.getBufferPos() ==
                            std::min(nbDown, (bigFileBufferNbLine - 1)) * bigFileBufferLineSize);
                }
            }
        }

        SECTION("With 1 command with n repetition")
        {
            WHEN("Moving Down by " << nbDown)
            {
                CommandManager::MoveCommand cmdNDown{{"", nbDown}, CommandManager::MoveCommandType::MoveDown};
                theApp.executeCommand(cmdNDown);
                theApp.refreshScreen();
                THEN("Cursor Pos should be moved")
                {
                    REQUIRE(theApp.getBufferPos() ==
                            std::min(nbDown, (bigFileBufferNbLine - 1)) * bigFileBufferLineSize);
                }
            }
        }
    }

    SECTION("Move fwd words")
    {
        SECTION("Move passed fist word")
        {
            auto startPos = GENERATE_COPY(range(0, 4));
            GIVEN("At start pos " << startPos)
            {
                theApp.setCursorBufferPosAndRefresh(startPos);
                CommandManager::MoveCommand cmdOneWordFwd{{"w", 1}, CommandManager::MoveCommandType::MoveWordFwd};
                theApp.executeCommand(cmdOneWordFwd);
                THEN("")
                {
                    REQUIRE(theApp.getBufferPos() == 5);
                }
            }
        }

        SECTION("Move some words")
        {
            auto [nbWord, resPos] = GENERATE(table<int, int>({
                {3, 12},
                {5, 29},
                {6, 31},
                {7, 36},
                {119, bigFileContent.length()-2 - 1},
                {120, bigFileContent.length()-2},
                {999, bigFileContent.length()-2},
            }) );
            WHEN("I move " << nbWord << " words")
            {
                CommandManager::MoveCommand cmdOneWordFwd{
                    {"w", nbWord}, CommandManager::MoveCommandType::MoveWordFwd};
                theApp.executeCommand(cmdOneWordFwd);
                THEN("I should be at " << resPos << " position in buffer.")
                    REQUIRE(theApp.getBufferPos() == resPos);
            }
        }
    }

    SECTION("Move Cursor Up passed end of window after scroll down")
    {
        GIVEN("We we set the buffer pos to first char of last line.")
        {
            theApp.setCursorBufferPosAndRefresh((bigFileBufferNbLine - 1) * bigFileBufferLineSize);

            auto nbUp = GENERATE_COPY(range(0, bigFileBufferNbLine + 4));

            SECTION("With n command")
            {
                CommandManager::MoveCommand cmdOneUp{{"", 1}, CommandManager::MoveCommandType::MoveUp};
                WHEN("Moving Up by " << nbUp)
                {
                    for (int i = 0; i < nbUp; i++)
                        theApp.executeCommand(cmdOneUp);
                    THEN("Cursor Pos should be moved")
                    {
                        REQUIRE(theApp.getBufferPos() ==
                                ((bigFileBufferNbLine - 1) - std::min(nbUp, (bigFileBufferNbLine - 1))) *
                                bigFileBufferLineSize);
                    }
                }
            }
            SECTION("With one command at n repetition")
            {
                WHEN("Moving Up by " << nbUp)
                {
                    CommandManager::MoveCommand cmdOneUp{{"", nbUp}, CommandManager::MoveCommandType::MoveUp};
                    theApp.executeCommand(cmdOneUp);
                    THEN("Cursor Pos should be moved")
                    {
                        REQUIRE(theApp.getBufferPos() ==
                                ((bigFileBufferNbLine - 1) - std::min(nbUp, (bigFileBufferNbLine - 1))) *
                                bigFileBufferLineSize);
                    }
                }
            }

        }

    }

    SECTION ("Delete stuff")
    {
        WHEN("Delete line")
        {
            auto posOnLine = GENERATE(range(0, 22));
            auto modifiedFileContent = bigFileContent;
            modifiedFileContent.erase(0, modifiedFileContent.find("\n") + 1);

            theApp.setCursorBufferPosAndRefresh(posOnLine);
            CommandManager::EditCommand cmdDeleteLine{{"", 1}, CommandManager::EditCommandType::Delete, {}};
            theApp.executeCommand(cmdDeleteLine);
            THEN("Line should be deleted")
            {
                REQUIRE(theApp.getFullBuffer() == modifiedFileContent);
            }

        }

        SECTION("Delete move word forward")
        {
            auto [nbWord, cutToPos] = GENERATE(table<int,int>({
                                                                      {1, 5},
                                                                      {2, 8},
                                                                      {5, 29},
                                                                      {6, 31},
                                                                      {119, 617},
                                                                      {120, 621},
                                                                      {121, 621},
            }));

            WHEN("Deleting " << nbWord << " from start of buffer")
            {
                auto modifiedFileContent = bigFileContent;
                modifiedFileContent.erase(0, cutToPos);
                if (modifiedFileContent.back() != '\n')
                    modifiedFileContent.push_back('\n');

                CommandManager::EditCommand cmdDeleteWords{
                        {"dw", nbWord},
                        CommandManager::EditCommandType::Delete,
                        {
                                {{"l", 1}, CommandManager::MoveCommandType::MoveWordFwd}}};
                theApp.executeCommand(cmdDeleteWords);
                THEN("characters should be deleted")
                {
                    REQUIRE(theApp.getFullBuffer() == modifiedFileContent);
                }
            }
        }
        SECTION("Delete move Line Up")
        {
            auto posStart = GENERATE(591, 592, 618);
            auto [nbRep, cutStart, cutEnd] = GENERATE(table<int, int, int>({
                                                                     {1, 558, 620},
                                                                     {2, 527, 620},
                                                                     {18, 31, 620},
                                                                     {19, 0, 620},
                                                                     {20, 0, 620},
                                                                     {21, 0, 620},
                                                             }));
            WHEN("Start position is " << posStart)
            {
                AND_WHEN("We delete linewise " << nbRep << " down")
                {
                    theApp.setCursorBufferPosAndRefresh(posStart);
                    CommandManager::EditCommand cmdDeleteChars{
                            {"dk", nbRep},
                            CommandManager::EditCommandType::Delete,
                            {
                                    {{"l", 1}, CommandManager::MoveCommandType::MoveUp}}};
                    theApp.executeCommand(cmdDeleteChars);
                    THEN("characters should be deleted")
                    {
                        auto modifiedFileContent = bigFileContent;
                        modifiedFileContent.erase(cutStart, cutEnd-cutStart+1);

                        REQUIRE(theApp.getFullBuffer() == modifiedFileContent);
                    }
                }
            }
        }
        SECTION("Delete move Line Down")
        {
            auto posStart = GENERATE(0UL, 1, 2, 3, 29);
            auto [nbRep, nbErase] = GENERATE(table<int, int>({
                                                                    {1, 62},
                                                                    {2, 93},
                                                                    {3, 124},
                                                                    {18, 589},
                                                                    {19, 620},
                                                                    {20, 620},
                                                                    {21, 620},
            }));
            WHEN("Start position is " << posStart)
            {
                AND_WHEN("We delete linewise " << nbRep << " down")
                {
                    theApp.setCursorBufferPosAndRefresh(posStart);
                    CommandManager::EditCommand cmdDeleteChars{
                            {"dk", nbRep},
                            CommandManager::EditCommandType::Delete,
                            {
                                    {{"l", 1}, CommandManager::MoveCommandType::MoveDown}}};
                    theApp.executeCommand(cmdDeleteChars);
                    THEN("characters should be deleted")
                    {
                        auto modifiedFileContent = bigFileContent;
                        modifiedFileContent.erase(0, nbErase);

                        REQUIRE(theApp.getFullBuffer() == modifiedFileContent);
                    }
                }
            }
        }

        SECTION("Delete move character backward")
        {
            auto posStart = GENERATE(0UL, 1, 2, 3, 29, 31, 32, bigFileContent.length()-2);
            WHEN("Buffer position is " << posStart)
            {
                unsigned long maxNdDelete = posStart;
                auto nbDelete = GENERATE_COPY(1, 2, 3, 10, 30, 32, maxNdDelete-1, maxNdDelete, maxNdDelete+1);
                AND_WHEN( "Deleting " << nbDelete <<" characters.")
                {
                    auto modifiedFileContent = bigFileContent;
                    if (posStart>=nbDelete)
                        modifiedFileContent.erase(posStart-nbDelete, nbDelete);
                    else if (posStart > 0)
                        modifiedFileContent.erase(0, posStart);

                    theApp.setCursorBufferPosAndRefresh(posStart);
                    CommandManager::EditCommand cmdDeleteChars{
                            {"d", nbDelete},
                            CommandManager::EditCommandType::Delete,
                            {
                                    {{"l", 1}, CommandManager::MoveCommandType::MoveLeft}}};
                    theApp.executeCommand(cmdDeleteChars);
                    THEN("characters should be deleted")
                    {
                        REQUIRE(theApp.getFullBuffer() == modifiedFileContent);
                    }
                    AND_THEN("Cursor should be at same pos")
                    {
                        REQUIRE(theApp.getBufferPos() == ((posStart>nbDelete) ? posStart-nbDelete : 0));
                    }
                }
            }
        }
        SECTION("Delete move chars Forward")
        {
            auto posStart = GENERATE(0UL, 1, 2, 3, 29, 31, 32, bigFileContent.length()-2);
            WHEN("Buffer position is " << posStart)
            {
                unsigned long maxNdDelete = std::max(1UL, bigFileContent.length() - 2 - posStart);
                auto nbDelete = GENERATE_COPY(1, 2, 3, 10, 30, 31, 32, maxNdDelete-1, maxNdDelete, maxNdDelete+1);
                AND_WHEN( "Deleting " << nbDelete <<" characters.")
                {
                    auto modifiedFileContent = bigFileContent;
                    modifiedFileContent.erase(posStart, nbDelete);
                    if (modifiedFileContent.back() != '\n')
                        modifiedFileContent.push_back('\n');

                    theApp.setCursorBufferPosAndRefresh(posStart);
                    CommandManager::EditCommand cmdDeleteChars{
                            {"d", nbDelete},
                            CommandManager::EditCommandType::Delete,
                            {
                                    {{"l", 1}, CommandManager::MoveCommandType::MoveRight}}};
                    theApp.executeCommand(cmdDeleteChars);
                    THEN("characters should be deleted")
                    {
                        REQUIRE(theApp.getFullBuffer() == modifiedFileContent);
                    }
                    AND_THEN("Cursor should be at same pos")
                    {
                        if (posStart==0)
                            REQUIRE(theApp.getBufferPos() == 0);
                        else if (modifiedFileContent[posStart] == '\n' && modifiedFileContent[posStart-1] != '\n')
                            REQUIRE(theApp.getBufferPos() == posStart-1);
                        else
                            REQUIRE(theApp.getBufferPos() == posStart);
                    }
                }
            }
        }
    }
}

TEST_CASE_METHOD(AppTestFixture, "Modify Buffer Tests", "[app]")
{
    int appNbLine = 10;
    WorkingCopy workingCopy{bigFileFullPath};
    VimApp theApp{40, appNbLine, workingCopy()};
    REQUIRE(theApp.getFullBuffer() == bigFileContent);

    CommandManager::ApplicationCommand saveCmd{{"", 1},CommandManager::AppCommandType::Save};
    SECTION("Check save same buffer")
    {
        theApp.executeCommand(saveCmd);
        REQUIRE(theApp.getFullBuffer() == bigFileContent);
        REQUIRE(readFile(workingCopy()) == bigFileContent);
    }
    SECTION("Check save empty buffer")
    {
        theApp.setFullBuffer("");
        theApp.executeCommand(saveCmd);
        REQUIRE(theApp.getFullBuffer() == "");
        REQUIRE(readFile(workingCopy()) == "");
    }

    CommandManager::ApplicationCommand saveAndQuitCmd{{"", 1},CommandManager::AppCommandType::SaveAndQuit};
    SECTION("Save and quit without modify")
    {
        theApp.executeCommand(saveAndQuitCmd);
        REQUIRE(theApp.getFullBuffer() == bigFileContent);
        REQUIRE(readFile(workingCopy()) == bigFileContent);
        REQUIRE(theApp.appIsDone());
    }
    SECTION("Save and quit  modify")
    {
        theApp.setFullBuffer("");
        theApp.executeCommand(saveAndQuitCmd);
        REQUIRE(theApp.getFullBuffer() == "");
        REQUIRE(readFile(workingCopy()) == "");
        REQUIRE(theApp.appIsDone());
    }

    CommandManager::ApplicationCommand quitCmd{{"", 1},CommandManager::AppCommandType::Quit};
    SECTION("Quit before saving")
    {
        theApp.setFullBuffer("");
        theApp.executeCommand(quitCmd);
        theApp.refreshScreen();
        REQUIRE(!theApp.appIsDone());
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Contains("error", Catch::CaseSensitive::No));
        theApp.putChar('l');
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Contains("error", Catch::CaseSensitive::No));
        theApp.putChar(':');
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Equals(":"));
        theApp.putChar('w');
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Equals(":w"));
        theApp.putChar('\n');
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Equals(""));

        theApp.executeCommand(quitCmd);
        REQUIRE(theApp.appIsDone());
    }
    CommandManager::ApplicationCommand quitHardCmd{{"", 1},CommandManager::AppCommandType::QuitHard};
    SECTION("Quit Hard no saving")
    {
        theApp.setFullBuffer("");
        theApp.executeCommand(quitCmd);
        REQUIRE(!theApp.appIsDone());
        theApp.executeCommand(quitHardCmd);
        REQUIRE(theApp.appIsDone());
    }

    CommandManager::ApplicationCommand reloadCmd{{"", 1},CommandManager::AppCommandType::Reload};
    SECTION("Reload with buffer modif")
    {
        theApp.setFullBuffer("");
        theApp.executeCommand(reloadCmd);
        theApp.refreshScreen();
        REQUIRE(theApp.getFullBuffer() == "");
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, Contains("error", Catch::CaseSensitive::No));
    }
    SECTION("Reload changed file with no buffer modif")
    {
        std::ofstream ofs(workingCopy.getPath(), std::ofstream::out | std::ofstream::trunc);
        ofs.close();

        theApp.executeCommand(reloadCmd);
        theApp.refreshScreen();
        REQUIRE(theApp.getFullBuffer() == "");
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, !Contains("error", Catch::CaseSensitive::No));
    }

    CommandManager::ApplicationCommand reloadHardCmd{{"", 1},CommandManager::AppCommandType::ReloadHard};
    SECTION("Reload Hard changed file with no buffer modif")
    {
        std::ofstream ofs(workingCopy.getPath(), std::ofstream::out | std::ofstream::trunc);
        ofs.close();

        theApp.executeCommand(reloadHardCmd);
        theApp.refreshScreen();
        REQUIRE(theApp.getFullBuffer() == "");
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, !Contains("error", Catch::CaseSensitive::No));
    }

    SECTION("Reload Hard changed file with buffer modif")
    {
        std::ofstream ofs(workingCopy.getPath(), std::ofstream::out | std::ofstream::trunc);
        ofs.close();

        theApp.setFullBuffer("My changes here");
        theApp.executeCommand(reloadHardCmd);
        theApp.refreshScreen();
        REQUIRE(theApp.getFullBuffer() == "");
        REQUIRE_THAT(theApp.getScreen().mLines.back().mStr, !Contains("error", Catch::CaseSensitive::No));
    }

    SECTION("delete back one char")
    {
        theApp.setCursorBufferPosAndRefresh(1);
        auto modifiedFileContent = bigFileContent;
        modifiedFileContent.erase(0, 1);
        CommandManager::EditCommand cmdDeleteChars{
                {"d", 1},
                CommandManager::EditCommandType::Delete,
                {
                        {{"l", 1}, CommandManager::MoveCommandType::MoveLeft}}};
        theApp.executeCommand(cmdDeleteChars);
        THEN("characters should be deleted")
        {
            REQUIRE(theApp.getFullBuffer() == modifiedFileContent);
        }
        AND_THEN("cursor pos should be ")
        {
            REQUIRE(theApp.getBufferPos() == 0);
        }
    }
    SECTION("Delete untill end of line")
    {
        auto modifiedFileContent = bigFileContent;
        modifiedFileContent.erase(0, modifiedFileContent.find('\n'));
        CommandManager::EditCommand cmdDeleteTilEndLine{
                {"d", 1},
                CommandManager::EditCommandType::Delete,
                {
                        {{"l", 1}, CommandManager::MoveCommandType::MoveToEndLine}}};
        theApp.executeCommand(cmdDeleteTilEndLine);
        THEN("line should be deleted but empty")
        {
            REQUIRE(theApp.getFullBuffer() == modifiedFileContent);
        }
    }
}

TEST_CASE_METHOD(AppTestFixture, "Bug test cases", "[app]")
{
    SECTION("Move in empty buffer crashes")
    {
        int nbLine = 10;
        VimApp theApp{40, nbLine};
        REQUIRE_NOTHROW(theApp.downArrow());
    }
    SECTION("Delete More than remaining lines")
    {
        int nbLine = 10;
        VimApp theApp{40, nbLine, smallFilePath};
        CommandManager::EditCommand cmdDeleteLine{{"", 9}, CommandManager::EditCommandType::Delete, {}};
        theApp.executeCommand(cmdDeleteLine);
        REQUIRE(theApp.getBufferPos() == 0);
    }
    SECTION("Delete last line when non empty cursor pos")
    {
        int nbLine = 10;
        VimApp theApp{40, nbLine, smallFilePath};
        theApp.setCursorBufferPosAndRefresh(2*smallFileBufferLineSize + 2);

        CommandManager::EditCommand cmdDeleteLine{{"", 1}, CommandManager::EditCommandType::Delete, {}};
        theApp.executeCommand(cmdDeleteLine);
        REQUIRE(theApp.getBufferPos() == smallFileBufferLineSize);

    }
    SECTION("Delete line when next line is empty")
    {
        auto fullFileName = myStaticFileDir / "deleteNextEmpty.txt";
        createFile(fullFileName,
                   "One Non Empty Line\n"
                   "\n"
                   "One Non Empty Line\n"
                   );
        VimApp anApp(50, 20, fullFileName);
        anApp.setCursorBufferPosAndRefresh(6);
        CommandManager::EditCommand cmdDeleteLine{{"", 1}, CommandManager::EditCommandType::Delete, {}};
        anApp.executeCommand(cmdDeleteLine);
        REQUIRE(anApp.getFullBuffer() == "\nOne Non Empty Line\n");
        REQUIRE(anApp.getBufferPos() == 0);
    }
}
