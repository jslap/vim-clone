//
// Created by Jean-Simon Lapointe on 2020-09-06.
//

#ifndef MYVIM_TESTUTIL_H
#define MYVIM_TESTUTIL_H

#include <filesystem>
#include <string>

namespace TestUtil
{
    std::filesystem::path createTmpDir();

    void createFile(std::filesystem::path const & filePath, std::string const & content = {});

    std::string readFile(std::filesystem::path const & filePath);

}


#endif //MYVIM_TESTUTIL_H
