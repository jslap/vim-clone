//
// Created by Jean-Simon Lapointe on 2020-09-13.
//

#ifndef MYVIM_VIMUTIL_H
#define MYVIM_VIMUTIL_H

#include <cstdint>
#include <string>

using u64 = std::uint64_t ;

class ScreenLine
{
public:
    std::string mStr;
    /// add some formating: cursor pos, selected,...
};


enum Mode
{
    Normal,
    Command,
};

using BufferPos = std::uint64_t;
using CharCode = std::uint64_t;

#endif //MYVIM_VIMUTIL_H
