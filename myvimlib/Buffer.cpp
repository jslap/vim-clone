//
// Created by Jean-Simon Lapointe on 2020-09-05.
//

#include "Buffer.h"

#include <iostream>
#include <fstream>
#include <algorithm>

#include <range/v3/view.hpp>
#include <range/v3/algorithm/count.hpp>

#include "spdlog/spdlog.h"

bool Buffer::save() const {
    if (!mBasePath.empty())
    {
        std::ofstream str{mBasePath};
        str << mContent;
        mIsOutdated = false;
        return true;
    }
    return false;
}

bool Buffer::load() {
    if (!mBasePath.empty())
    {
        {
            std::ifstream str{mBasePath};
            mContent = std::string((std::istreambuf_iterator<char>(str)),
                                   std::istreambuf_iterator<char>());
        }
        mIsOutdated = false;
        return true;
    }
    return false;
}

std::string Buffer::getContent() const {
    return mContent;
}

Buffer::Buffer(const std::filesystem::path &p) : mBasePath(p)
{
    load();
}

void Buffer::setContent(std::string const &content) {
    if (mContent != content)
    {
        mContent = content;
        mIsOutdated = true;
    }
}

const std::filesystem::path &Buffer::getPath() const {
    return mBasePath;
}

int Buffer::nbLine() const
{
    if (mContent.empty())
        return 0;
    else
    {
        auto nbNewLine = ranges::count(mContent, '\n');
        if (mContent.back() != '\n')
            return nbNewLine+1;
        else
            return nbNewLine;
    }
}

std::string Buffer::getLine(int lineNum) const
{
    if (mContent.empty())
        return {};
    auto line = mContent | ranges::views::split('\n') | ranges::views::drop(lineNum) | ranges::views::take(1) | ranges::to<std::vector<std::string>>();
    if (line.empty())
        return {};
    return line.front();
}

std::pair<int, int> Buffer::getLineNumAndOffsetForBufferOffset(int bufOffset) const
{
    auto cumulLineLengths = mContent
                            | ranges::views::split('\n')
                            | ranges::views::transform([](auto const & s){return ranges::to<std::string>(s).length() + 1;})
                            | ranges::views::partial_sum
                            | ranges::views::take_while([&bufOffset](auto const n){return n < bufOffset+1;})
                            | ranges::to<std::vector<int>>;

    if (cumulLineLengths.empty())
        return {0,bufOffset};
    return {cumulLineLengths.size(), bufOffset-cumulLineLengths.back()};
}

int Buffer::getBufferOffsetFromStartOfLine(int lineNum) const
{
    auto cumulLineLengths = mContent
                            | ranges::views::split('\n')
                            | ranges::views::transform([](auto const & s){return ranges::to<std::string>(s).length() + 1;})
                            | ranges::views::partial_sum
                            | ranges::views::take_exactly(lineNum)
                            | ranges::to<std::vector<int>>;

    if (cumulLineLengths.empty())
        return 0;
    return cumulLineLengths.back();
}

std::optional<u64> Buffer::match(std::regex const &pattern, u64 fromOffset, u64 numSubMatch) const
{
    if (fromOffset >= mContent.length())
        return {};
    std::smatch res;
    bool isMatch = std::regex_search(std::next(mContent.begin(), fromOffset), mContent.end(), res, pattern);
    if (isMatch)
        return fromOffset + res.position(numSubMatch);
    return {};
}

void Buffer::deleteChunk(BufferPos posStart, BufferPos posEnd)
{
    if (posStart > posEnd)
        throw std::logic_error("deleteChunk: end is before start");
    if (posEnd >= getBufferLength())
        throw std::logic_error("deleteChunk: chunk is passed the end");
    mIsOutdated = true;
    mContent.erase(posStart, posEnd-posStart+1);
}

int Buffer::getLineLength(int lineNum)
{
    return getLine(lineNum).length();
}

BufferPos Buffer::getBufferPosOfPosLineStart(BufferPos pos) const
{
    auto lineNum = getLineNumAndOffsetForBufferOffset(pos).first;
    return getBufferOffsetFromStartOfLine(lineNum);
}

BufferPos Buffer::getBufferPosOfPosLineEnd(BufferPos pos) const
{
    auto lineNum = getLineNumAndOffsetForBufferOffset(pos).first;
    return getBufferOffsetFromStartOfLine(lineNum+1);
}
