//
// Created by Jean-Simon Lapointe on 2020-09-13.
//

#ifndef MYVIM_COMMANDMANAGER_H
#define MYVIM_COMMANDMANAGER_H

#include "VimUtil.h"

#include <vector>
#include <string>
#include <optional>
#include <variant>

class CommandManager
{
public:
    CommandManager() = default;

    enum class MoveCommandType
    {
        MoveRight,
        MoveLeft,
        MoveUp,
        MoveDown,
        MoveToCharInLine,
        MoveToBeginLine,
        MoveToEndLine,
        MoveToSearch,
        MoveWordFwd,
        MoveWordBack,
    };
    enum class EditCommandType
    {
        Delete
    };
    enum class AppCommandType
    {
        Quit,
        QuitHard,
        Reload,
        ReloadHard,
        Save,
        SaveAndQuit
    };

    class CommandBase
    {
    public:
        std::string mCommandString;
        int mRepetition;
    };
    class MoveCommand : public CommandBase
    {
    public:
        MoveCommandType mCommandType;
        std::string mArgument;
    };

    class EditCommand : public CommandBase
    {
    public:
        enum EditCommandType mCommandType;
        std::optional<MoveCommand> mApplyTo;
    };

    class ApplicationCommand : public CommandBase
    {
    public:
        enum AppCommandType mCommandType;
    };

    using GenCommand = std::variant<MoveCommand, EditCommand, ApplicationCommand>;
    using ReturnedCommand = std::optional<GenCommand>;
    ReturnedCommand putChar(CharCode const & c);
    ReturnedCommand escapeKey();
    ReturnedCommand rightArrow();
    ReturnedCommand leftArrow();
    ReturnedCommand upArrow();
    ReturnedCommand downArrow();

    Mode getMode() const {return mMode;}

    ScreenLine getCommandLine() const;

    using CharVec = std::vector<CharCode>;
    using CharVecRange = std::pair<CharVec::const_iterator, CharVec::const_iterator>;

private:
    ReturnedCommand normalModePutChar(CharCode const & c);
    ReturnedCommand commandModePutChar(CharCode const & c);
    ReturnedCommand executeCommandBuffer();

    using CmdBuffer = std::vector<CharCode>;
    CmdBuffer mCommandBuffer;
    Mode mMode{Normal};
    std::optional<EditCommand> mEditLastCommand{};

    void updateLastEditCommand(ReturnedCommand const &lastCommand);
};


#endif //MYVIM_COMMANDMANAGER_H
