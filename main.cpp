#define _XOPEN_SOURCE_EXTENDED

#include <locale.h>
#include <ncurses.h>
#include <stdlib.h>

#include "fmt/chrono.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"

#include "myvimlib/VimApp.h"


int
main(int argc, char *argv[])
{
    std::filesystem::path startFile{};
    if (argc >= 2)
        startFile = argv[1];

    auto fileLogger = spdlog::basic_logger_mt("basic_logger", "logs/myvim-log.txt");
    fileLogger->flush_on(spdlog::level::debug);
    spdlog::set_level(spdlog::level::debug);
    spdlog::set_default_logger(fileLogger);


    initscr();			/* Start curses mode 		*/
    raw();				/* Line buffering disabled	*/
    keypad(stdscr, TRUE);		/* We get F1, F2 etc..		*/
    noecho();			/* Don't echo() while we do getch */
    clear();

    VimApp app{COLS, LINES, startFile};
    auto lastTime = std::chrono::system_clock::now();
    while (!app.appIsDone())
    {
        auto const & curScreen = app.getScreen();
        for (int i = 0; i < curScreen.mLines.size(); ++i)
        {
            std::string fullLine{curScreen.mLines[i].mStr};
            fullLine += std::string(COLS-(fullLine.length()-1), ' ');
            mvprintw(i, 0, "%s", fullLine.c_str());
        }
        auto curPos = app.getCursorPos();
        move(curPos.y, curPos.x);
        refresh();
        spdlog::info("Cur Buffer Pos {}. Cursor pos: ({},{})", app.getBufferPos(), app.getCursorPos().x, app.getCursorPos().y);

        auto curTime = std::chrono::system_clock::now();
        spdlog::info("Time for loop: {}", std::chrono::duration_cast<std::chrono::milliseconds>(curTime-lastTime));
        auto c = getch();
        lastTime = std::chrono::system_clock::now();

        spdlog::info("Char types: {}", c);
        if (c == 3) // ctrl-c
        {
            spdlog::info("Main CTRL-C sequence: Quitting");
            break;
        }
        switch (c)
        {
            case 27:
                app.escapeKey();
                break;
            case KEY_LEFT:
                app.leftArrow();
                break;
            case KEY_RIGHT:
                app.rightArrow();
                break;
            case KEY_UP:
                app.upArrow();
                break;
            case KEY_DOWN:
                app.downArrow();
                break;
            default:
                app.putChar(c);
                break;
        }
    }

    endwin();
    return EXIT_SUCCESS;
}
